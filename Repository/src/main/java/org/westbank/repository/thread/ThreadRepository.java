package org.westbank.repository.thread;

import org.westbank.repository.Repository;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class ThreadRepository extends Repository {

    public ThreadRepository() {

    }

    private ConcurrentHashMap<String, Object> thread_repository;

    private ThreadRepositoryFactory thread_repository_factory;

    public ThreadRepositoryFactory getThreadRepositoryFactory() {
        return thread_repository_factory;
    }

    @Override
    public Repository createRepositorialInstance() {
        thread_repository = new ConcurrentHashMap<String, Object>();
        thread_repository_factory = new ThreadRepositoryFactory();
        return this;
    }
}
