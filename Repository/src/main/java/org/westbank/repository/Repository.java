package org.westbank.repository;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 *     Class designated to hold all cached information for use of the @link{ServerInstance}.
 **/
public abstract class Repository {

    /**
     * Default Construtor
     */
    public Repository() {
        logger.info("Initializing Server Repository..");
        repository_of_repositories = new ConcurrentHashMap<String, Object>();
    }

    /** Acts as a super() for extending classes. **/
    public Repository(String repository_name, Object repository) {
        /** Sets the repository name. **/
        this.repository_name = repository_name;
        /** Checks if the repository being created is already in the repository. **/
        if(!repository_of_repositories.contains(repository))
            /** Registers the repository if it is not in the @link{ConcurrentHashMap}. **/
            registerRepository(repository_name, repository);
        /** Warns us that something went from and the object is trying to be instiantated twice. **/
        else logger.warn("Attempting to create a repository over an existing: " + repository_name);
    }

    /** The @link{ConcurrentHashMap} object. **/
    protected ConcurrentHashMap<String, Object> repository_of_repositories;

    /** Registers a repository to the @link{ConcurrentHashMap}. **/
    public Object registerRepository(String repository_name, Object repository) {
        Repository repo = (Repository) repository;
        /** Checks if the repository we are attempting to add is existing. **/
        if(!repository_of_repositories.contains(repo)) {
            /** Instiantates the repository object. **/
            repo.createRepositorialInstance();
            /** Returns the addition of the repository. **/
            return repository_of_repositories.put(repository_name, repository);
        }
        /** Warns that the repository is existent. **/
        logger.warn("Repository is existing : " + repository_name);
        /** Returns null for now because we do not want to register over an existing repository. **/
        return null;
    }

    /** Gets the repository based on the simple name defined in the super(). **/
    public Object getRepository(String repository_name) {
        /** Makes sure the requested repository is actually in the @link{ConcurrentHashMap}. **/
        if(repository_of_repositories.containsKey(repository_name))
            /** Returns the repository from the @link{ConcurrentHashMap}. **/
            return repository_of_repositories.get(repository_name);
        /** Warns us that no repository is existent. **/
        logger.warn("No repository is existing for : " + repository_name);
        /** Returns null as there is no repository in the @link{ConcurrentHashMap}. **/
        return null;
    }

    /** Instiantes the @link{Repository} object. **/
    public abstract Repository createRepositorialInstance();

    /** Return the repository of repositories. **/
    public ConcurrentHashMap<String, Object> getRepository() {
        return repository_of_repositories;
    }

    @XStreamAlias("repository_name")
    private String repository_name;

    /** Returns the @link{Repository} name. **/
    public String getRepositoryName() { return repository_name; }

    /** Creates a new @link{LoggerFactory} for this class @link{Repository}. **/
    private Logger logger = LoggerFactory.getLogger(Repository.class);

}
