package org.westbank.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.westbank.repository.world.WorldRepository;

import java.util.Arrays;
import java.util.List;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class RepositoryManager extends Repository {

    /**
     * Default Constructor.
     * @param configuration
     */
    public RepositoryManager(Object configuration) {
        this.server_configuration = configuration;
    }

    /**
     * Constructs the repository based on the @link{XMLConfiguration}'s List<Repository>.
     * @param repository_list
     * @return
     */
    public void constructRepository(List<Repository> repository_list) {
        if(repository_list == null) {
            /** Warns that we have failed to construct the repository list. **/
            logger.warn("Failed to construct the repository list.");
            /** Returns if nothing is in the list. **/
            return;
        }
        /** Loops through the @link{List<Repository>}. **/
        repository_list.forEach((repo) -> {
            /** For each entry in the @link{List} we register it as a new @link{Repository}. **/
            registerRepository(repo.getRepositoryName(), repo);
        });
    }

    /**
     * Constructs the @link{WorldRepository} object.
     * @param world_list
     * @return registers each world defined in the @link{XMLConfiguration}.
     */
    public void constructWorldRepository(List<?> world_list) {
        if(world_list == null) {
            /** Warns us that the worlds list has failed to construct. **/
            logger.warn("Failed to construct the worlds list.");
            /** Returns null because there was nothing in the @link{world_list}. **/
            return;
        }
        /** Creates a local variable for the @link{Repository} @link{WorldRepository}. **/
        WorldRepository world_repository = (WorldRepository) getRepository("WorldRepository");
        /** Loops through the @link{List<World>}. **/
        world_list.forEach((world) -> {
            /** For each entry in the @link{world_list} we register it as a new @link{World}. **/
            world_repository.registerWorlds(world.getClass().getSimpleName(), world);
        });
    }

    /** @link{ServerConfiguration} Object. **/
    private Object server_configuration;

    /** Returns the @link{ServerConfiguration}. }**/
    public Object getServerConfiguration() { return server_configuration; }

    /** Logging SL4J Object for this class' instance.**/
    private Logger logger = LoggerFactory.getLogger(RepositoryManager.class);

    @Override
    public Repository createRepositorialInstance() {
        return null;
    }
}
