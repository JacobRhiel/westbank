package org.westbank.repository.world;

import org.westbank.repository.Repository;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 *     Holds all created world instances.
 **/
public class WorldRepository extends Repository {

    /**
     * Default constructor.
     */
    public WorldRepository() {
        super(WorldRepository.class.getSimpleName(), WorldRepository.class);
    }

    /**
     * Instiantates the repository object.
     * @return
     */
    @Override
    public Repository createRepositorialInstance() {
        /** Creates the @link{ConcurrentHashMap} to represent this repositorys' cached data. **/
        world_repository = new ConcurrentHashMap<String, Object>();
        /** Returns this object. **/
        return this;
    }

    /**
     * Registers multiple world instances into the collections.
     * @param world_name
     * @param world -> all worlds in the array.
     * @return
     */
    public Object registerWorlds(String world_name, Object... world) {
        /** Checks it any of the worlds we are registering is already in the repository. **/
        if(!world_repository.contains(world)) {
            /** Loops through the {Object... world} array.**/
            for (Object w : world)
            /** Registers each world if they relate. **/
                return registerWorld(world_name, w);
        }
        /** If nothing is related, we return null and handle from there. **/
        return null;
    }

    /**
     * Registered one selected world.
     * @param world_name
     * @param world
     * @return
     */
    public Object registerWorld(String world_name, Object world) {
        /** Checks if the object is null and it is not existing in the repository. **/
        if(world != null && !world_repository.contains(world))
            /** Submits the world object into the registry map. **/
            return world_repository.put(world_name, world);
        /** Returns null for handling from this point. **/
        else return null;
    }

    /**
     * Returns the registered world we are requesting.
     * @param world_name
     * @return
     */
    public Object getRegisteredWorld(String world_name) {
        /** Loops through the repository.**/
        for(Object world : world_repository.entrySet())
            /** Gets all of the 'key' names of the worlds in the repository. **/
            for(String name : world_repository.keySet())
                /** Checks if the name in the repository is equal to the world name we are requesting. **/
                if(name == world_name)
                    /** Returns the world object we are requesting. **/
                    return world;
        /** If the world is not found in the repository we return null. **/
        return null;
    }

    /**
     * HashMap holding all world object instances.
     */
    private ConcurrentHashMap<String, Object> world_repository;

    public ConcurrentHashMap getWorldRepository() {
        return world_repository;
    }

}
