package org.westbank.js5;

import org.westbank.network.session.Session;

import java.util.HashMap;
import java.util.Map;

public enum Js5Prot {

    REQUEST_PREFETCH(0),
    REQUEST_URGENT(1),
    LOGIN_NOTIFY(2),
    LOGOUT_NOTIFY(3),
    SETUP_XOR(4),
    CONN_INIT(6),
    DROP_QUEUE(7);

    private static Map<Integer, Js5Prot> idMap;

    private int id;
    private Class<? extends Session> sessionClass;

    public int getId() {
        return id;
    }

    public Class<? extends Session> getSessionClass() {
        return sessionClass;
    }

    Js5Prot(int id) {

        this.id = id;

    }

    public static Js5Prot getById(int id) {
        if ( idMap == null ) {
            idMap = new HashMap<Integer, Js5Prot>();
            for ( Js5Prot p : Js5Prot.values() )
                idMap.put(p.getId(), p);
        }
        return idMap.get(id);
    }
}
