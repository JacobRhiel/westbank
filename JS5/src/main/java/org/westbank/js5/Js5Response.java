package org.westbank.js5;

public enum Js5Response {
    OK(0),
    OUT_OF_DATE(6),
    FULL_1(7),
    FULL_2(9);

    private int id;

    Js5Response(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
