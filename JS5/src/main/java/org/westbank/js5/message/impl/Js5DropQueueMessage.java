package org.westbank.js5.message.impl;

import org.westbank.js5.message.Js5ClientMessage;

public class Js5DropQueueMessage implements Js5ClientMessage {

    @Override
    public String toString() {
        return "Js5DropQueueMessage{}";
    }
}
