package org.westbank.js5.message.impl;

import org.westbank.js5.message.Js5ClientMessage;

import java.nio.ByteBuffer;

public class Js5RequestMessage implements Js5ClientMessage {

    private final boolean urgent;
    private final int archive;
    private final int group;
    private ByteBuffer data;

    public Js5RequestMessage(boolean urgent, int archive, int group) {
        this.urgent = urgent;
        this.archive = archive;
        this.group = group;
    }

    public boolean isUrgent() {
        return urgent;
    }

    public int getArchive() {
        return archive;
    }

    public int getGroup() {
        return group;
    }

    @Override
    public String toString() {
        return "Js5RequestMessage{" +
                "urgent=" + urgent +
                ", archive=" + archive +
                ", group=" + group +
                '}';
    }
}
