package org.westbank.js5.message.impl;

import io.netty.buffer.ByteBuf;
import org.westbank.js5.message.Js5ServerMessage;

import java.nio.ByteBuffer;

public class Js5DataMessage implements Js5ServerMessage {
    private final int offset;
    private final ByteBuf source;
    private final int size;

    public Js5DataMessage(int offset, ByteBuf source, int size) {
        this.offset = offset;
        this.source = source;
        this.size = size;
    }

    public int getOffset() {
        return offset;
    }

    public ByteBuf getSource() {
        return source;
    }

    public int getSize() {
        return size;
    }
}
