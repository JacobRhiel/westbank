package org.westbank.js5.message.impl;

import org.westbank.js5.message.Js5ClientMessage;

public class Js5StatusMessage implements Js5ClientMessage {
    private boolean loggedIn;

    public Js5StatusMessage(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    @Override
    public String toString() {
        return "Js5StatusMessage{" +
                "loggedIn=" + loggedIn +
                '}';
    }
}
