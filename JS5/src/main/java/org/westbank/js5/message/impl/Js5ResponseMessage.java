package org.westbank.js5.message.impl;

import org.westbank.js5.Js5Response;
import org.westbank.js5.message.Js5ServerMessage;

public class Js5ResponseMessage implements Js5ServerMessage {

    private Js5Response response;

    public Js5ResponseMessage(Js5Response response) {
        this.response = response;
    }

    public Js5Response getResponse() {
        return response;
    }
}
