package org.westbank.js5.message.impl;

import org.westbank.js5.message.Js5ClientMessage;

public class Js5HandshakeMessage implements Js5ClientMessage {

    /**
     * The version reported by the client
     */
    private int version;

    public Js5HandshakeMessage(int version) {
        this.version = version;
    }

    public int getVersion() {
        return version;
    }

    @Override
    public String toString() {
        return "Js5HandshakeMessage{" +
                "version=" + version +
                '}';
    }
}
