package org.westbank.js5.message.impl;

import io.netty.buffer.ByteBuf;
import org.westbank.js5.message.Js5ServerMessage;

import java.nio.ByteBuffer;

public class Js5InitDataMessage implements Js5ServerMessage {

    private final int archive;
    private final int group;
    private final ByteBuf source;
    private final boolean urgent;
    public Js5InitDataMessage(boolean urgent,
                              int archive,
                              int group,
                              ByteBuf source
                              //int size,
                              //int compression,
                              //int fileSize
                              ) {
        this.archive = archive;
        this.group = group;
        this.source = source;
        this.urgent = urgent;
    }

    public int getArchive() {
        return archive;
    }

    public int getGroup() {
        return group;
    }

    public ByteBuf getSource() {
        return source;
    }

    public boolean isUrgent() {
        return urgent;
    }
}
