package org.westbank.js5.message.impl;

import org.westbank.js5.message.Js5ClientMessage;

public class Js5InitMessage implements Js5ClientMessage {

    @Override
    public String toString() {
        return "Js5InitMessage{}";
    }
}
