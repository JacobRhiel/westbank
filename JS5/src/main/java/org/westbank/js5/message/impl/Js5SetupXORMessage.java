package org.westbank.js5.message.impl;

import org.westbank.js5.message.Js5ClientMessage;

public class Js5SetupXORMessage implements Js5ClientMessage {

    private int key;

    public Js5SetupXORMessage(int key) {
        this.key = key;
    }

    public int getKey() {
        return key;
    }

    @Override
    public String toString() {
        return "Js5SetupXORMessage{" +
                "key=" + key +
                '}';
    }
}
