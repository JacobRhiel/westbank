package org.westbank.js5.message.impl;

import org.westbank.js5.message.Js5ServerMessage;

import java.util.List;

public class Js5WeightMessage implements Js5ServerMessage {

    private List<Integer> prefetchWeights;

    public Js5WeightMessage(List<Integer> prefetchWeights) {
        this.prefetchWeights = prefetchWeights;
    }

    public List<Integer> getPrefetchWeights() {
        return prefetchWeights;
    }
}
