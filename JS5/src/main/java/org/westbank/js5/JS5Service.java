package org.westbank.js5;

import org.westbank.configuration.service.Service;
import org.westbank.server.instance.ServerInstance;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class JS5Service extends Service {

    public JS5Service() {
        super("JS5Remote", 15, true);
    }

    @Override
    public Object createInstance(Object server) {
        return new JS5ServiceInstance((ServerInstance)server, this);
    }

    private int worker_count = 1;

    public int getWorkerCount() { return worker_count; }
}
