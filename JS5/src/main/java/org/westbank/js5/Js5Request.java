package org.westbank.js5;

import io.netty.buffer.ByteBuf;
import org.westbank.js5.message.impl.Js5RequestMessage;

import java.nio.ByteBuffer;

public class Js5Request {

    private final boolean urgent;
    private final int archive;
    private final int group;
    private int position = 0;
    private ByteBuf data;
    private int compression;
    private int fileSize;
    private int contentSize;

    public Js5Request(Js5RequestMessage message) {
        urgent = message.isUrgent();
        archive = message.getArchive();
        group = message.getGroup();
    }

    public boolean isUrgent() {
        return urgent;
    }

    public int getArchive() {
        return archive;
    }

    public int getGroup() {
        return group;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int currentBlock) {
        this.position = currentBlock;
    }

    public void setData(ByteBuf data) {
        this.data = data;
    }

    public ByteBuf getData() {
        return data;
    }

    @Override
    public String toString() {
        return "Js5Request{" +
                "urgent=" + urgent +
                ", archive=" + archive +
                ", group=" + group +
                ", position=" + position +
                ", data=" + data +
                ", compression=" + compression +
                ", fileSize=" + fileSize +
                ", contentSize=" + contentSize +
                '}';
    }

    public void setCompression(int compression) {
        this.compression = compression;
    }

    public int getCompression() {
        return compression;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setContentSize(int contentSize) {
        this.contentSize = contentSize;
    }

    public int getContentSize() {
        return contentSize;
    }
}
