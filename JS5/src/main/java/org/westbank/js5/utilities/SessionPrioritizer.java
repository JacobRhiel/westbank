package org.westbank.js5.utilities;

import org.westbank.js5.session.JS5Session;

import java.util.Comparator;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class SessionPrioritizer implements Comparator<JS5Session> {

    @Override
    public int compare(JS5Session o1, JS5Session o2) {
        boolean o1l, o2l;
        o1l = o1.isLoggedIn();
        o2l = o2.isLoggedIn();
        if ( o1l == o2l )
            return 0;
        else if ( o1l )
            return -1;
        else
            return 1;
    }

}
