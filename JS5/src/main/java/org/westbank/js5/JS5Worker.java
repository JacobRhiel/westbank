package org.westbank.js5;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import org.westbank.js5.message.impl.Js5DataMessage;
import org.westbank.js5.message.impl.Js5InitDataMessage;
import org.westbank.js5.session.JS5Session;
import org.westbank.js5.session.pipeline.Js5Encoder;
import org.westbank.js5.utilities.SessionPrioritizer;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class JS5Worker implements Runnable {

    /**
     * Sets the initial capacity for the queue
     */
    private static final int QUEUE_INIT_CAP = 512;

    /**
     * Queue of sessions requiring service
     */
    private final PriorityBlockingQueue<JS5Session> serviceQueue;

    /**
     * Service instance we are running for
     */
    private final JS5ServiceInstance service;

    /**
     * Whether this worker is running and should continue running.
     */
    private volatile boolean running = false;

    /**
     * Debugging variable that holds the number of detected race conditions
     */
    private int raceCounter = 0;

    /**
     * The thread executing this worker
     */
    private Thread thread = null;

    /**
     * Creates a new worker
     * @param service The service we are working for
     */
    public JS5Worker(JS5ServiceInstance service) {
        this.service = service;
        this.serviceQueue = new PriorityBlockingQueue<>(QUEUE_INIT_CAP,
                new SessionPrioritizer());
    }

    /**
     * Enqueues session for processing
     * @param session The session that needs processing
     */
    public void enqueue(JS5Session session){
        if (!this.serviceQueue.contains(session))
            serviceQueue.add(session);
    }

    /**
     * Gets the service this worker is working for
     * @return The service, never null.
     */
    public JS5ServiceInstance getService() {
        return service;
    }

    /**
     * Gets the amount of sessions currently on the service queue.
     * @return The number of sessions awaiting processing
     */
    public int getQueueCount() {
        return serviceQueue.size();
    }

    /**
     * Sets the thread this worker is running on
     * @param thread The thread ( may be null )
     */
    public void setThread(Thread thread) {
        this.thread = thread;
    }

    /**
     * Stops the worker
     */
    public void shutdown() {
        if ( ! running )
            return;

        running = false;

        if ( thread != null ){
            try {
                thread.interrupt();
                thread.join();
            } catch (InterruptedException ignored) {
            }
        }

    }

    /**
     * Checks whether the worker is running, returning false if the
     * worker has not yet been started, if the worker has been shut down
     * or when the worker has died as result of an unhandled exception.
     * @return Whether the worker is running
     */
    public boolean isRunning() {
        return running;
    }

    @Override
    public void run() {
        JS5Session session;

        running = true;

        try {
            while ( running ) {

                try {

                    //This blocks when queue is empty so no delays required here.
                    session = serviceQueue.take();

                } catch (InterruptedException e) {
                    continue;
                }

                processSession(session);

            }
        } finally {

            running = false;

        }
    }

    private void processSession(JS5Session session) {
        session.lock();
        try {
            Js5Request request = session.getActiveRequest();

            if ( request == null )
                request = session.getUrgentQueue().poll();

            if ( request == null )
                request = session.getPrefetchQueue().poll();

            if ( request != null )
                processRequest( session, request );
            else
                raceCounter++;//This is for debugging only, to count race conditions.

        } finally {
            session.unlock();
        }

    }

    private void processRequest(JS5Session session, Js5Request request) {
        if (request.getPosition() == 0) {
            int size = request.getFileSize();
            if (size >= Js5Encoder.BLOCK_SIZE - 3)
                size = Js5Encoder.BLOCK_SIZE - 3;
                Js5InitDataMessage idm = new Js5InitDataMessage(request.isUrgent(),
                        request.getArchive(),
                        request.getGroup(),
                        request.getData());
                request.setPosition(size);
                session.setActiveRequest(request);
                session.getChannel().writeAndFlush(idm).addListener(new NextBlockFutureListener(session));
        } else {
            int pos = request.getPosition();
            int size = request.getFileSize() - pos;
            if (size > (Js5Encoder.BLOCK_SIZE - 1))
                size = Js5Encoder.BLOCK_SIZE - 1;
            Js5DataMessage dm = new Js5DataMessage(request.getPosition(), request.getData(), size);
            request.setPosition(size + pos);
            session.getChannel().writeAndFlush(dm).addListener(new NextBlockFutureListener(session));
        }
    }

    class NextBlockFutureListener implements ChannelFutureListener {

        private final JS5Session ses;

        public NextBlockFutureListener(JS5Session session) {
            ses = session;
        }

        @Override
        public void operationComplete(ChannelFuture future) throws Exception {
            ses.lock();
            try {
                Js5Request req = ses.getActiveRequest();
                    int pos = req.getPosition();
                    int size = req.getFileSize() - pos;
                    if (size > (Js5Encoder.BLOCK_SIZE - 1))
                        size = Js5Encoder.BLOCK_SIZE - 1;
                    if (size <= 0) {
                        ses.setActiveRequest(null);
                        if (!ses.isIdle())
                            service.requestService(ses);
                    } else {
                        service.requestService(ses);
                    }

            } finally {
                ses.unlock();
            }

        }

    }

}
