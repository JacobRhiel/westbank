package org.westbank.js5;

import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.westbank.js5.session.JS5Session;
import org.westbank.network.session.Session;
import org.westbank.server.instance.ServerInstance;
import org.westbank.server.instance.ServiceInstance;
import org.westbank.store.StoreInterface;

import java.util.Arrays;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class JS5ServiceInstance extends ServiceInstance {

    /**
     * The logger
     */
    private final Logger logger = LoggerFactory.getLogger(JS5ServiceInstance.class);

    /**
     * The number of workers to use
     */
    private final int workerCount;

    /**
     * The workers
     */
    private JS5Worker workers[];

    /**
     * The store the data is fetched from
     */
    private StoreInterface store;

    public JS5ServiceInstance(ServerInstance server, JS5Service service) {
        super(server, service);

        workerCount = service.getWorkerCount();

        workers = new JS5Worker[workerCount];

        for ( int i = 0; i < workerCount; i++ )
            startWorker(i);

        store = (StoreInterface) server.getService("Store Service");

    }

    /**
     * Assigns a session to a worker for processing
     * @param session the service that needs processing
     */
    public void requestService(JS5Session session ) {
        JS5Worker l = null;
        int _c,c = Integer.MAX_VALUE;

        for ( JS5Worker w : workers ) {
            if ( w == null || !w.isRunning())
                continue;
            _c = w.getQueueCount();
            if ( _c < c ) {
                c = _c;
                l = w;
            }
        }

        if ( l == null )
            l = startWorker(0);//THIS SHOULD NEVER EVER EVER HAPPEN

        l.enqueue(session);

    }

    @Override
    protected boolean handleThreadException(Thread t, Throwable e) {

        logger.error("JS5 service worker died, restarting", e);

        for ( int i = 0; i < workerCount; i++ ) {
            if ( workers[i] == null || !workers[i].isRunning() ) {
                startWorker(i);
            }
        }

        return true;
    }

    private JS5Worker startWorker(int i) {
        JS5Worker worker = new JS5Worker(this);
        Thread thread = createThread( worker );
        thread.setName("JS5Worker #"+i);
        worker.setThread(thread);
        thread.start();
        workers[i] = worker;
        return worker;
    }

    @Override
    public Session createSession(Channel channel) {
        return new JS5Session(this, channel);
    }

    @Override
    public void shutdown() {

        for (JS5Worker w : workers){
            if ( w != null)
                w.shutdown();
        }

    }


    public StoreInterface getStore() {
        return store;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {

    }
}
