package org.westbank.js5.session.pipeline;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.westbank.js5.session.JS5Session;
import org.westbank.network.channel.ChannelHandler;
import org.westbank.network.session.Session;

public class Js5XOREncoder extends MessageToByteEncoder<ByteBuf> {

    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf msg, ByteBuf out) throws Exception {

        Session _session = ctx.channel().attr(ChannelHandler.session_key).get();

        if ( _session == null || ! (_session instanceof JS5Session) ) {
            throw new InternalError("Programmer error!: Js5XOREncoder on non JS5 channel!");
        }

        JS5Session session = (JS5Session) _session;
        int key = session.getXORKey();

        while (msg.isReadable()) {
            out.writeByte(msg.readUnsignedByte() ^ key);
        }

    }

}
