package org.westbank.js5.session;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import org.westbank.js5.JS5ServiceInstance;
import org.westbank.js5.Js5Request;
import org.westbank.js5.Js5Response;
import org.westbank.js5.message.impl.*;
import org.westbank.js5.session.pipeline.Js5Decoder;
import org.westbank.js5.session.pipeline.Js5Encoder;
import org.westbank.js5.session.pipeline.Js5HandshakeDecoder;
import org.westbank.js5.session.pipeline.Js5XOREncoder;
import org.westbank.network.session.Session;
import org.westbank.openrs.openrs.Cache;
import org.westbank.openrs.openrs.ChecksumTable;
import org.westbank.openrs.openrs.Container;
import org.westbank.openrs.store.StoreServiceInstance;
import org.westbank.server.Server;
import org.westbank.server.instance.ServerInstance;
import org.westbank.store.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class JS5Session extends Session {

    private ArrayList<Integer> weights = new ArrayList<>(16);
    private boolean loggedIn = false;
    private BlockingQueue<Js5Request> urgentQueue = new LinkedBlockingQueue<>();
    private BlockingQueue<Js5Request> prefetchQueue = new LinkedBlockingQueue<>();
    private int xorKey = 0;
    private JS5ServiceInstance service;
    private Js5Request activeRequest;
    private Lock lock = new ReentrantLock();

    public JS5Session(JS5ServiceInstance service, Channel channel) {
        super(channel);
        this.service = service;
        channel.pipeline().addAfter("ConnectionDecoder", "JS5HS", new Js5HandshakeDecoder());
        channel.pipeline().addAfter("JS5HS", "XOR", new Js5XOREncoder());
        channel.pipeline().addAfter("XOR", "JS5Encoder", new Js5Encoder());
    }

    @Override
    public void messageReceived(Object msg) {
        if ( msg instanceof Js5HandshakeMessage) {
            if ( ((Js5HandshakeMessage) msg).getVersion() != 120 ) {
                getChannel().writeAndFlush(new Js5ResponseMessage(Js5Response.OUT_OF_DATE));
                getChannel().close();
            } else {
                getChannel().pipeline().addAfter("JS5Encoder", "JS5Decoder", new Js5Decoder());
                getChannel().writeAndFlush(new Js5ResponseMessage(Js5Response.OK));
            }
        } else if ( msg instanceof Js5StatusMessage ) {
            loggedIn = ((Js5StatusMessage) msg).isLoggedIn();
        } else if ( msg instanceof Js5DropQueueMessage ) {
            urgentQueue.clear();
            prefetchQueue.clear();
        } else if ( msg instanceof Js5RequestMessage ) {
            handleIncomingRequest((Js5RequestMessage) msg);
        } else if ( msg instanceof Js5SetupXORMessage ) {
            xorKey = ((Js5SetupXORMessage) msg).getKey();
        }
    }

    private void handleIncomingRequest(Js5RequestMessage msg) {
        final Js5Request req = new Js5Request(msg);
        StoreInterface store = service.getStore();
        StoreRequest request = new StoreRequest(RequestType.GET_RAW,
                msg.getArchive(),
                msg.getGroup(),
                -1,
                null);
        request.setHandler(request1 -> {
            if (request1.getStatus() == RequestStatus.ERROR)
                handleError(req, request1.getCause());
            else if (request1.getStatus() == RequestStatus.DONE) {
                ByteBuf buf = Unpooled.wrappedBuffer(request1.getData());

                ByteBuf buf1 = Unpooled.buffer();

                int type = req.getArchive();
                int file = req.getGroup();
                int compression = buf.readUnsignedByte();
                int size = buf.readInt();
                buf1.writeByte(type);
                buf1.writeShort(file);
                buf1.writeByte(compression);
                buf1.writeInt(size);

                int bytes = buf.readableBytes();

                if (bytes > 504) bytes = 504;

                buf1.writeBytes(buf.readBytes(bytes));

                for (;;) {
                    bytes = buf.readableBytes();
                    if (bytes == 0) break;
                    else if (bytes > 511) bytes = 511;
                    buf1.writeByte(0xFF);
                    buf1.writeBytes(buf.readBytes(bytes));
                }

                req.setData(buf1);

                if (!req.isUrgent()) compression |= 0x80;

                req.setCompression(compression);
                req.setContentSize(size);
                req.setFileSize(size + (compression == 0 ? 5 : 9));

                lock();
                try {
                    boolean wasIdle = isIdle();
                    if (req.isUrgent()) urgentQueue.add(req);
                    else prefetchQueue.add(req);
                    if (activeRequest == null && wasIdle) service.requestService(JS5Session.this);
                } finally {
                    unlock();
                }
            }
        });
        store.postRequest(request);
    }

    private void handleError(Js5Request msg, Throwable cause) {
        System.err.println("Error:" + cause);
        cause.printStackTrace();
    }

    public BlockingQueue<Js5Request> getUrgentQueue() {
        return urgentQueue;
    }

    public BlockingQueue<Js5Request> getPrefetchQueue() {
        return prefetchQueue;
    }

    public int getXORKey() {
        return xorKey;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public boolean isIdle() {
        return urgentQueue.size() == 0 && prefetchQueue.size() == 0;
    }

    public void lock() {
        lock.lock();
    }

    public void unlock() {
        lock.unlock();
    }

    public void setActiveRequest(Js5Request activeRequest) {
        this.activeRequest = activeRequest;
    }

    public Js5Request getActiveRequest() {
        return activeRequest;
    }

}
