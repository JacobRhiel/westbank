package org.westbank.js5.session.pipeline;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.westbank.js5.message.impl.Js5HandshakeMessage;

import java.util.List;

public class Js5HandshakeDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf byteBuf, List<Object> list) throws Exception {
        ChannelPipeline pipeline = ctx.pipeline();

        if (!byteBuf.isReadable() || byteBuf.readableBytes() < 4)
            return;

        list.add( new Js5HandshakeMessage( byteBuf.readInt() ) );

        if(byteBuf.isReadable())
            list.add(byteBuf.readBytes(byteBuf.readableBytes()));

        pipeline.remove(Js5HandshakeDecoder.class);

    }

}
