package org.westbank.js5.session.pipeline;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.westbank.js5.message.*;
import org.westbank.js5.message.impl.*;

public class Js5Encoder extends MessageToByteEncoder<Js5ServerMessage> {

    public static final int BLOCK_SIZE = 512;

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext,
                          Js5ServerMessage msg,
                          ByteBuf byteBuf) throws Exception {
        if (msg instanceof Js5ResponseMessage) {
            byteBuf.writeByte(((Js5ResponseMessage) msg).getResponse().getId());
        } else if (msg instanceof Js5InitDataMessage) {
            Js5InitDataMessage idm = (Js5InitDataMessage) msg;
            int type = idm.getSource().getUnsignedByte(0);
            int file = idm.getSource().getShort(1);
            int compression = idm.getSource().getUnsignedByte(3);
            int file_size = idm.getSource().getInt(4);
            byteBuf.writeBytes(idm.getSource());
        } else if ( msg instanceof Js5DataMessage ) {
            Js5DataMessage dm = (Js5DataMessage) msg;
            //byteBuf.writeByte(-1);
            dm.getSource().writerIndex(dm.getOffset());
            dm.getSource().capacity(dm.getOffset()+dm.getSize());
            byteBuf.writeBytes(dm.getSource());
        } else {
            throw new InternalError("Programmer Error!: Unencoded JS5 message");
        }

    }
}
