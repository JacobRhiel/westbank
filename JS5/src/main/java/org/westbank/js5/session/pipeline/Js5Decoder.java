package org.westbank.js5.session.pipeline;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.westbank.js5.Js5Prot;
import org.westbank.js5.message.*;
import org.westbank.js5.message.impl.*;

import java.util.List;

public class Js5Decoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf byteBuf, List<Object> list) throws Exception {

        Js5ClientMessage msg;

        if (!byteBuf.isReadable() || byteBuf.readableBytes() < 4)
            return;

        int op = byteBuf.readByte();

        Js5Prot prot = Js5Prot.getById(op);

        if ( prot == null )
            throw new IllegalArgumentException( "Invalid JS5 request: "+ op );

        switch ( prot ) {
            case REQUEST_PREFETCH:
                int prefetch_uid = byteBuf.readUnsignedMedium();
                msg = new Js5RequestMessage(false, (prefetch_uid >> 16), (prefetch_uid & 0xFFFF));
                break;
            case REQUEST_URGENT:
                int uid = byteBuf.readUnsignedMedium();
                msg = new Js5RequestMessage(true, (uid >> 16), (uid & 0xFFFF));
                break;
            case LOGIN_NOTIFY:
                byteBuf.readMedium();
                msg = new Js5StatusMessage(true);
                break;
            case LOGOUT_NOTIFY:
                byteBuf.readMedium();
                msg = new Js5StatusMessage(false);
                break;
            case CONN_INIT:
                byteBuf.readUnsignedMedium();
                msg = new Js5InitMessage();
                break;
            case DROP_QUEUE:
                byteBuf.readUnsignedMedium();
                msg = new Js5DropQueueMessage();
                break;
            case SETUP_XOR:
                int key = byteBuf.readUnsignedByte();
                byteBuf.readUnsignedShort();
                msg = new Js5SetupXORMessage(key);
                break;
            default:
                throw new InternalError("Programmer error! JS5 Message not decoded!");

        }

        list.add(msg);

        if(byteBuf.isReadable())
            list.add(byteBuf.readBytes(byteBuf.readableBytes()));

    }
}