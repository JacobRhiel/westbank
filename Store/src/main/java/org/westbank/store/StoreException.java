package org.westbank.store;

/**
 * An exception for the store
 *
 * Created: 28/03/16
 *
 * @author Peter Bosch <peterbosc@gmail.com>
 */
public class StoreException extends Exception {

    public StoreException(String message, Throwable cause) {
        super(message, cause);
    }
}
