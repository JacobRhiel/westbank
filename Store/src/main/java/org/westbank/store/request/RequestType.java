package org.westbank.store.request;

/**
 * The operations supported by the store service
 *
 * Created: 22/03/16
 *
 * @author Peter Bosch <peterbjornx@peterbjornx.nl>
 */
public enum RequestType {

    /**
     * The request is to add or modify a group in the backing storage.
     */
    PUT_GROUP,

    /**
     * The request is to add or modify a single file in
     * a group on backing storage.
     */
    PUT_FILE,

    /**
     * The request is to retrieve a group from backing storage.
     */
    GET_GROUP,

    /**
     * The request is to retrieve a file from backing storage.
     */
    GET_FILE,

    /**
     * The request is to retrieve a group from backing storage without
     * decoding the container.
     */
    GET_RAW

}
