package org.westbank.store.request;

import org.westbank.store.event.RequestHandler;

import java.nio.ByteBuffer;

public class StoreRequest {

    private final RequestType type;

    /**
     * The archive this request should act on
     */
    private final int archive;

    /**
     * The group this request should act on
     */
    private final int group;

    /**
     * The file this request should act on
     */
    private final int file;

    /**
     * The keys to use when accessing the file
     */
    private final int[] keys;

    private RequestStatus status = RequestStatus.POSTED;

    /**
     * The exception that caused the status to be set to ERROR,
     * The value of this field is undefined if the status any
     * other than ERROR
     */
    private Throwable cause = null;

    /**
     * The ByteBuffer containing resulting from or to be used by
     * the requested action
     */
    private ByteBuffer data;

    /**
     * The handler that will be invoked when the status of the
     * request changes
     */
    private RequestHandler handler;

    /**
     * Creates a new StoreRequest
     * @param type The operation to perform
     * @param archive The archive to operate on
     * @param group The group to operate on
     * @param file The file to operate on
     * @param keys The keys to use
     */
    public StoreRequest(RequestType type, int archive, int group, int file, int[] keys) {
        this.type = type;
        this.archive = archive;
        this.group = group;
        this.file = file;
        if ( keys != null )
            this.keys = keys;
        else
            this.keys = new int[]{0,0,0,0};
    }

    /**
     * Returns the registered handler for status changes
     * @return the handler, may be null
     */
    public RequestHandler getHandler() {
        return handler;
    }

    /**
     * Registers a handler for status changes
     * Only one handler can be registered at the same time
     * @param handler The handler to call when the request status changes
     */
    public void setHandler(RequestHandler handler) {
        this.handler = handler;
    }

    /**
     * Changes the status of the request and calls the status change
     * handler. Must not be used to set the ERROR status
     * @param status The new status of the request
     */
    public void setStatus(RequestStatus status){
        assert status != RequestStatus.ERROR;
        this.status = status;
        this.cause = null;
        if ( handler != null )
            handler.onStateChange(this);
    }

    /**
     * Sets the ERROR status on the request, sets the cause for
     * that error and calls the state change handler.
     * @param cause The cause of the error
     */
    public void setError(Throwable cause){
        this.status = RequestStatus.ERROR;
        this.cause = cause;
        if ( handler != null )
            handler.onStateChange(this);
    }

    public RequestType getType() {
        return type;
    }

    /**
     * Gets the archive the requested action should work on
     * @return the id of the archive
     */
    public int getArchive() {
        return archive;
    }

    /**
     * Gets the group the requested action should work on
     * @return the id of the archive
     */
    public int getGroup() {
        return group;
    }

    /**
     * Gets the file the requested action should work on
     * @return the id of the archive.
     */
    public int getFile() {
        return file;
    }

    /**
     * Gets the current status of the request
     * @return the status of the request
     */
    public RequestStatus getStatus() {
        return status;
    }

    /**
     * Gets the exception that caused the ERROR status,
     * undefined result if the status is not ERROR.
     * @return The exception that caused the ERROR status.
     */
    public Throwable getCause() {
        return cause;
    }
    /**
     * Gets the keys to be used to encrypt or decrypt
     * the group, not used in GET_RAW.
     * @return the XTEA keys.
     */
    public int[] getKeys() {
        return keys;
    }

    /**
     * Gets the data resulting from the operation
     * Only set after a GET_FILE, GET_GROUP or GET_RAW request.
     * @return the data.
     */
    public ByteBuffer getData() {
        return data;
    }

    /**
     * Sets the data to be used in the operation.
     * Only used in a PUT_FILE or PUT_GROUP request.
     * @param data The data to be stored
     */
    public void setData(ByteBuffer data) {
        this.data = data;
    }
}
