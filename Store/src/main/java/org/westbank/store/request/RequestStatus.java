package org.westbank.store.request;

/**
 * The possible states of a StoreRequest
 *
 * Created: 22/03/16
 *
 * @author Peter Bosch <peterbosc@gmail.com>
 */
public enum RequestStatus {

    /**
     * The request has been posted for processing by the worker.
     */
    POSTED,

    /**
     * The worker is currently executing the requested action.
     */
    PROCESSING,

    /**
     * The worker has finished processing the request, if the
     * request yielded data the data will be available in the
     * field on the request
     */
    DONE,

    /**
     * The worker has encountered a problem while processing
     * the request, the exception that caused this status to
     * be set will be available in the cause field of the
     * request.
     */
    ERROR

}
