package org.westbank.store;


import java.nio.ByteBuffer;

/**
 * Describes the interface a store provider offers.
 *
 * Created: 28/03/16
 *
 * @author Peter Bosch <peterbosc@gmail.com>
 */
public interface StoreInterface {

    /**
     * The internal name of this interface, used to get the providing service from the server
     */
    public static final String NAME = "store";

    /**
     * Synchronously request a group from the store
     * The actual processing is still done on a worker thread so this method will not incur
     * CPU usage on the calling thread
     * @param archive The archive to fetch the group from
     * @param group The id of the group to fetch
     * @param keys The decryption keys for the group ( null if not encrypted )
     * @return A ByteBuffer containing the file data
     * @throws StoreException if an exception occurred during processing it will be rethrown
     */
    public ByteBuffer getGroup(int archive, int group, int[] keys) throws StoreException;

    /**
     * Synchronously request a file from the store
     * The actual processing is still done on a worker thread so this method will not incur
     * CPU usage on the calling thread
     * @param archive The archive to fetch the group from
     * @param group The id of the group containing this file
     * @param file The id of the file to fetch
     * @param keys The decryption keys for the group ( null if not encrypted )
     * @return A ByteBuffer containing the file data
     * @throws StoreException if an exception occurred during processing it will be rethrown
     */
    public ByteBuffer getFile(int archive, int group, int file, int[] keys) throws StoreException;

    /**
     * Synchronously request a group from the store without decoding it into a Container
     * The actual processing is still done on a worker thread so this method will not incur
     * CPU usage on the calling thread
     * @param archive The archive to fetch the group from
     * @param group The id of the group to fetch
     * @return A byte buffer containing the raw data of the group
     * @throws Throwable if an exception occurred during processing it will be rethrown
     */
    public ByteBuffer getRaw(int archive, int group) throws Throwable;

    /**
     * Synchronously request a file from the store
     * The actual processing is still done on a worker thread so this method will not incur
     * CPU usage on the calling thread
     * @param archive The archive to fetch the group from
     * @param group The id of the group containing this file
     * @param file The id of the file to fetch
     * @return A ByteBuffer containing the file data
     * @throws StoreException if an exception occurred during processing it will be rethrown
     */
    public ByteBuffer getFile(int archive, int group, int file) throws StoreException;

    /**
     * Synchronously request a group from the store
     * The actual processing is still done on a worker thread so this method will not incur
     * CPU usage on the calling thread
     * @param archive The archive to fetch the group from
     * @param group The id of the group to fetch
     * @return A ByteBuffer containing the file data
     * @throws StoreException if an exception occurred during processing it will be rethrown
     */
    public ByteBuffer getGroup(int archive, int group) throws StoreException;

    /**
     * Schedules a request to be handled by the service
     * Because the request will be handled asynchronously, the service
     * offers the possibility to register a callback to monitor
     * status changes.
     * @param request The request to handle
     */
    public void postRequest(StoreRequest request);

}
