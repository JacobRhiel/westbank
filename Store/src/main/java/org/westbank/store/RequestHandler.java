package org.westbank.store;

/**
 * The interface for handling StoreRequest
 * state changes
 *
 * Created: 25/03/16
 *
 * @author Peter Bosch <peterbjornx@peterbjornx.nl>
 */
public interface RequestHandler {

    /**
     * This method is called when the state of
     * a request has changed
     * @param request The request of which the state changed.
     */
    void onStateChange(StoreRequest request);

}
