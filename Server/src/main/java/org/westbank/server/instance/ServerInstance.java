package org.westbank.server.instance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.westbank.configuration.service.Service;
import org.westbank.server.Server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class ServerInstance {

    public ServerInstance(Server server) {
        this.server = server;
        this.thread_group = new ThreadGroup("ServerInstance Thread Group");
        logger.info("Initializing Server..");
    }

    public Object instantiate(Object instantiated_object) {
        return instantiated_object;
    }

    public void instantiateServices() {
        List<Service> services = server.getServerConfiguration().getServiceConfiguration().getServices();
        for(Service service : services) {
            try {
                ServiceInstance service_instance = (ServiceInstance) service.createInstance(this);
                int identifier = service.getIdentifier();
                network_service_map.put(identifier, service_instance);
                name_service_map.put(service.getServiceName(), service_instance);
            } catch(Exception e ) {
                e.printStackTrace();
            }
        }

    }

    private static Server server;

    public static Server getServer() {
        return server;
    }

    private ThreadGroup thread_group;

    public ThreadGroup getThreadGroup() { return thread_group; }

    private ConcurrentHashMap<Integer, ServiceInstance> network_service_map = new ConcurrentHashMap<>();

    private ConcurrentHashMap<String, ServiceInstance> name_service_map = new ConcurrentHashMap<>();

    public ServiceInstance getService(int key) {
        return network_service_map.get(key);
    }

    public List<String> getServiceList() {
        List<String> name_map_list = name_service_map.keySet().stream().collect(Collectors.toList());
        return name_map_list;
    }

    public ServiceInstance getService(String key) {
        return name_service_map.get(key);
    }

    private Logger logger = LoggerFactory.getLogger(ServerInstance.class);

}
