package org.westbank.server.instance;

import io.netty.channel.Channel;
import org.westbank.configuration.service.Service;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public abstract class ServiceInstance implements Thread.UncaughtExceptionHandler {

    public ServiceInstance(ServerInstance server, Service service) {
        String tgn;
        this.server = server;
        this.service = service;
        tgn = "Thread group for: \"" + service.getServiceName() + "\"";
        this.thread_group = new ThreadGroup(server.getThreadGroup(), tgn);
    }

    protected Thread createThread( Runnable target ) {
        Thread thread =  new Thread(thread_group, target);
        thread.setUncaughtExceptionHandler(this);
        return thread;
    }

    public abstract Object createSession(Channel channel);

    public abstract void shutdown();

    protected boolean handleThreadException(Thread t, Throwable e) {
        return false;
    }

    protected Service service;

    protected ServerInstance server;

    protected ThreadGroup thread_group;

}
