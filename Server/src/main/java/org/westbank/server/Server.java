package org.westbank.server;

import org.westbank.configuration.ServerConfiguration;
import org.westbank.repository.RepositoryManager;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 *     Creates the @link{Server} object.
 **/
public class Server {

    /** Default constructor
     * @link(ServerConfiguration} Initializes the XML configuration.
     * **/
    public Server(ServerConfiguration configuration) {
       this(configuration, new RepositoryManager(configuration));
    }

    /** Creates a new @link{Server} instance defining @link{ServerConfiguration} and the @link{RepositoryManager}. **/
    public Server(ServerConfiguration configuration, RepositoryManager repository_manager) {
        this.configuration = configuration;
        this.repository_manager = repository_manager;
    }

    /** Server Configuration **/
    private ServerConfiguration configuration;

    /** Returns the @link{ServerConfiguration} object. **/
    public ServerConfiguration getServerConfiguration() { return configuration; }

    /** Repository Manager **/
    private RepositoryManager repository_manager;

    /** Returns the @link{RepositoryManager}. **/
    public RepositoryManager getRepositoryManager() {
        return repository_manager;
    }

}
