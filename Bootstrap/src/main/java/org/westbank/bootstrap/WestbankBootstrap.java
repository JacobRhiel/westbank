package org.westbank.bootstrap;

import org.westbank.configuration.ServerConfiguration;
import org.westbank.configuration.XMLConfiguration;
import org.westbank.configuration.world.World;
import org.westbank.network.WestbankNetwork;
import org.westbank.repository.Repository;
import org.westbank.repository.world.WorldRepository;
import org.westbank.server.Server;
import org.westbank.server.instance.ServerInstance;

import java.util.List;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 *     The Bootstrap that initializes the entire server.
 **/
public class WestbankBootstrap {

    public static void main(String[] args) {

        /** Creates the @link{ServerConfiguration} Object. **/
        ServerConfiguration server_configuration = new XMLConfiguration();

        /** Creates the @link{Server} Object with the @link(server_configuration} instance. **/
        Server server = new Server(server_configuration);

        /** Creates the @link{ServerInstance} Object with the @link{server} instance. **/
        ServerInstance server_instance = new ServerInstance(server);

        /** TODO: Temporary.**/
        /** Constructs a new repository list. **/
        List<Repository> repository_list = server_configuration.getRepositoryConfiguration().getRepositories();

        /** TODO: Temporary.**/
        /** Constructs a new world list. **/
        List<World> world_list = server_configuration.getWorldConfiguration().getWorlds();

        /** Constructs the repository of repositories based on the @link{repository_list} Object. **/
        server_instance.getServer().getRepositoryManager().constructRepository(repository_list);

        /** Constructs the world repository based on the @link{world_list} Object. **/
        server_instance.getServer().getRepositoryManager().constructWorldRepository(world_list);

        System.out.println(server_instance.getServer().getRepositoryManager().getRepository().entrySet());

        /** TODO: Temporary. **/
        /** A @link{WorldRepository} Object representing the @link{Repository} we find the the list. **/
        WorldRepository repo = (WorldRepository) server_instance.getServer().getRepositoryManager().getRepository("WorldRepository");

        System.out.println(repo.getWorldRepository().entrySet());

        /** Creates a new {@link WestbankNetwork} instance with the {@link ServerInstance} object as its argument. **/
        WestbankNetwork westbank_network = new WestbankNetwork(server_instance);

        /** Instantiates the {@link ServerInstance} based on the {@link WestbankNetwork} bootstrap configuration. **/
        server_instance.instantiate(westbank_network.constructBootstrap());

        /** Instantiates the list of {@link org.westbank.configuration.service.Service}'s defined by the {@link ServerConfiguration}. **/
        server_instance.instantiateServices();

    }

}
