package org.westbank.network;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.MultithreadEventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.westbank.configuration.ServerConfiguration;
import org.westbank.network.channel.child.ChildHandler;
import org.westbank.server.instance.ServerInstance;

import java.net.InetSocketAddress;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class WestbankNetwork {

    public WestbankNetwork(ServerInstance server) {
        this(server, new ServerBootstrap());
    }

    public WestbankNetwork(ServerInstance server, ServerBootstrap bootstrap) {
        this.server = server;
        this.configuration = server.getServer().getServerConfiguration();
        this.bootstrap = bootstrap;
        this.group = new NioEventLoopGroup(Runtime.getRuntime().availableProcessors());
    }

    public ServerBootstrap constructBootstrap() {
        bootstrap.group(group)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChildHandler(server))
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.TCP_NODELAY, true);
        return bind();
    }

    public ServerBootstrap bind() {
        try {
            InetSocketAddress address = configuration.getNetworkConfiguration().getNetwork().getSocketAddress();
            Channel channel = bootstrap.bind(address).sync().channel();
            logger.info("Bound channel : " + address + "," + channel);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
        return bootstrap;
    }

    private ServerBootstrap bootstrap;

    private ChannelFuture future;

    private MultithreadEventLoopGroup group;

    private ServerInstance server;

    private ServerConfiguration configuration;

    private Logger logger = LoggerFactory.getLogger(WestbankNetwork.class);

}
