package org.westbank.network.message;

import org.westbank.network.session.Session;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class ServiceMessage implements ChannelMessage {

    public ServiceMessage(Session session, int service_identifier) {
        this.session = session;
        this.service_identifier = service_identifier;
    }

    private final Session session;

    public Session getSession() {
        return session;
    }

    private final int service_identifier;

    public int getServiceIdentifier() { return service_identifier; }

}
