package org.westbank.network.session;

import io.netty.channel.Channel;
import org.westbank.network.message.ServiceMessage;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public abstract class Session {

    public Session(Channel channel) {
        this.channel = channel;
    }

    public abstract void messageReceived(Object msg);

    private final Channel channel;

    public Channel getChannel() { return channel; }

    public void initialized(ServiceMessage msg) {

    }

}
