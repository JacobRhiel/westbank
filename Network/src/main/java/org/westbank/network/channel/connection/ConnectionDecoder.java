package org.westbank.network.channel.connection;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.westbank.network.message.ServiceMessage;
import org.westbank.network.session.Session;
import org.westbank.server.instance.ServerInstance;
import org.westbank.server.instance.ServiceInstance;

import java.util.List;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class ConnectionDecoder extends ByteToMessageDecoder {

    public ConnectionDecoder(ServerInstance server) {
        this.server = server;
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

        if(!in.isReadable()) return;

        ChannelPipeline pipeline = ctx.pipeline();

        int connection_type = in.readUnsignedByte();

        ByteBuf remaining;

        if(in.isReadable()) remaining = in.readBytes(in.readableBytes());
        else remaining = null;

        ServiceInstance svc = server.getService(connection_type);

        if(svc == null) throw new UnsupportedOperationException("Unimplemented service requested: " + connection_type);

        Session session = (Session) svc.createSession(ctx.channel());

        ServiceMessage message = new ServiceMessage(session, connection_type);

        out.add(message);

        if(remaining != null) out.add(remaining);

        pipeline.remove(this);

    }

    private ServerInstance server;

}
