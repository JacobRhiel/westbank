package org.westbank.network.channel.child;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.string.StringDecoder;
import org.westbank.network.channel.ChannelHandler;
import org.westbank.network.channel.connection.ConnectionDecoder;
import org.westbank.server.instance.ServerInstance;

import java.nio.charset.Charset;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class ChildHandler extends ChannelInitializer<NioSocketChannel> {

    public ChildHandler(ServerInstance server) {
        this.server = server;
    }

    @Override
    protected void initChannel(NioSocketChannel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();
        pipeline.addFirst("ConnectionDecoder", new ConnectionDecoder(server));
        pipeline.addLast("ChannelHandler", new ChannelHandler());
    }

    private ServerInstance server;

}
