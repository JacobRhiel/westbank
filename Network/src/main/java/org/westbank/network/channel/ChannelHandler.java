package org.westbank.network.channel;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.AttributeKey;
import org.westbank.network.message.ServiceMessage;
import org.westbank.network.session.Session;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class ChannelHandler extends SimpleChannelInboundHandler {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {

        Session session = ctx.channel().attr(session_key).get();

        if(msg instanceof ServiceMessage) {

            session = ((ServiceMessage) msg).getSession();

            ctx.channel().attr(session_key).set(session);

            session.initialized((ServiceMessage) msg);

        } else if(session != null)
            session.messageReceived(msg);
          else
            throw new InternalError("Programmer error! A message was received that did not have a session to go with it");
    }

    @Override
    public boolean acceptInboundMessage(Object msg) throws Exception {
        return super.acceptInboundMessage(msg);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        ctx.channel().close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        System.err.println("Error in Channel:" + cause);
        cause.printStackTrace();
        ctx.close();
    }

    public static final AttributeKey<Session> session_key = AttributeKey.valueOf("session");

}
