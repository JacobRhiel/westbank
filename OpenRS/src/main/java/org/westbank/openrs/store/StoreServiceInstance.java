package org.westbank.openrs.store;

import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.westbank.network.session.Session;
import org.westbank.openrs.openrs.Cache;
import org.westbank.openrs.openrs.FileStore;
import org.westbank.server.instance.ServerInstance;
import org.westbank.server.instance.ServiceInstance;
import org.westbank.store.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A running instance of the store service
 *
 * @author Peter Bosch <peterbjornx@peterbjornx.nl>
 */
public class StoreServiceInstance extends ServiceInstance implements StoreInterface {

    /**
     * The logger
     */
    private final Logger logger = LoggerFactory.getLogger(StoreServiceInstance.class);

    /**
     * The cache backing this store
     */
    private static Cache cache;

    public static Cache getCache() {
        return cache;
    }

    /**
     * The worker handling the store requests
     */
    private StoreWorker worker;

    /**
     * An instance of the store service
     * @param server The server instance we are running on
     * @param service The service being instantiated
     */
    public StoreServiceInstance(ServerInstance server, StoreService service) {
        super(server, service);
        try {
            cache = new Cache(FileStore.open(service.getStorePath()));
            worker = new StoreWorker(this, cache);
            Thread thread = new Thread(worker);
            worker.setThread(thread);
            thread.setName("Store worker");
            thread.start();
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize store service", e);
        }
    }

    private void blockingPostRequest(StoreRequest request) throws StoreException {
        final Lock lock = new ReentrantLock();
        final Condition ready = lock.newCondition();
        request.setHandler(request1 -> {
            lock.lock();
            try {
                ready.signal();
            } finally {
                lock.unlock();
            }
        });
        lock.lock();
        try {

            while ( request.getStatus() != RequestStatus.ERROR &&
                    request.getStatus() != RequestStatus.DONE )
                try {
                    ready.await();
                } catch (InterruptedException ignored) {
                    System.out.println("Exception");
                }

            if ( request.getStatus() == RequestStatus.ERROR )
                throw new StoreException("An error occurred in the worker", request.getCause());

        } finally {
            lock.unlock();
        }
    }

    /**
     * Synchronously request a group from the store
     * The actual processing is still done on a worker thread so this method will not incur
     * CPU usage on the calling thread
     * @param archive The archive to fetch the group from
     * @param group The id of the group to fetch
     * @param keys The decryption keys for the group ( null if not encrypted )
     * @return The container containing the group
     * @throws StoreException if an exception occurred during processing it will be rethrown
     */
    public ByteBuffer getGroup(int archive, int group, int[] keys) throws StoreException {
        StoreRequest request = new StoreRequest(RequestType.GET_GROUP,
                archive,
                group,
                -1,
                keys);
        blockingPostRequest(request);
        return request.getData();
    }

    /**
     * Synchronously request a file from the store
     * The actual processing is still done on a worker thread so this method will not incur
     * CPU usage on the calling thread
     * @param archive The archive to fetch the group from
     * @param group The id of the group containing this file
     * @param file The id of the file to fetch
     * @param keys The decryption keys for the group ( null if not encrypted )
     * @return A ByteBuffer containing the file data
     * @throws StoreException if an exception occurred during processing it will be rethrown
     */
    public ByteBuffer getFile(int archive, int group, int file, int[] keys) throws StoreException {
        StoreRequest request = new StoreRequest(RequestType.GET_FILE,
                archive,
                group,
                file,
                keys);
        blockingPostRequest(request);
        return request.getData();
    }

    /**
     * Synchronously request a group from the store without decoding it into a Container
     * The actual processing is still done on a worker thread so this method will not incur
     * CPU usage on the calling thread
     * @param archive The archive to fetch the group from
     * @param group The id of the group to fetch
     * @return A byte buffer containing the raw data of the group
     * @throws StoreException if an exception occurred during processing it will be rethrown
     */
    public ByteBuffer getRaw(int archive, int group) throws StoreException {
        StoreRequest request = new StoreRequest(RequestType.GET_RAW,
                archive,
                group,
                -1,
                null);
        blockingPostRequest(request);
        return request.getData();
    }

    /**
     * Synchronously request a file from the store
     * The actual processing is still done on a worker thread so this method will not incur
     * CPU usage on the calling thread
     * @param archive The archive to fetch the group from
     * @param group The id of the group containing this file
     * @param file The id of the file to fetch
     * @return A ByteBuffer containing the file data
     * @throws StoreException if an exception occurred during processing it will be rethrown
     */
    public ByteBuffer getFile(int archive, int group, int file) throws StoreException {
        return getFile( archive, group, file, null );
    }

    /**
     * Synchronously request a group from the store
     * The actual processing is still done on a worker thread so this method will not incur
     * CPU usage on the calling thread
     * @param archive The archive to fetch the group from
     * @param group The id of the group to fetch
     * @return The container containing the group
     * @throws StoreException if an exception occurred during processing it will be rethrown
     */
    public ByteBuffer getGroup(int archive, int group) throws StoreException {
        return getGroup( archive, group, null );
    }

    public void postRequest(StoreRequest request)
    {
        worker.enqueue(request);
    }

    /**
     * Not implemented. We are not a network service
     * @param channel The channel associated with that connection
     * @return null
     */
    @Override
    public Session createSession(Channel channel) {
        return null;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {

    }

    @Override
    public void shutdown() {
        worker.shutdown();
        try {
            cache.close();
        } catch (IOException e) {
            logger.warn("Exception while closing cache", e);
        }
    }

}
