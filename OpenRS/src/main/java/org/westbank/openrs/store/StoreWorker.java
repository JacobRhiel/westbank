package org.westbank.openrs.store;

import org.westbank.openrs.openrs.Cache;
import org.westbank.openrs.openrs.Container;
import org.westbank.store.RequestStatus;
import org.westbank.store.StoreRequest;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class StoreWorker implements Runnable{

    /**
     * Sets the initial capacity for the queue
     */
    private static final int QUEUE_INIT_CAP = 512;

    /**
     * Queue of requests
     */
    private BlockingQueue<StoreRequest> serviceQueue;

    /**
     * Service instance we are running for
     */
    private StoreServiceInstance service;

    /**
     * Whether this worker is running and should continue running.
     */
    private volatile boolean running = false;

    /**
     * Debugging variable that holds the number of detected race conditions
     */
    private int raceCounter = 0;

    /**
     * The thread executing this worker
     */
    private Thread thread = null;

    /**
     * The cache we will be serving from
     */
    private Cache cache;

    /**
     * Creates a new worker
     * @param service The service we are working for
     * @param cache The cache we are storing our data in
     */
    public StoreWorker(StoreServiceInstance service, Cache cache) {
        this.service = service;
        this.serviceQueue = new LinkedBlockingQueue<>(QUEUE_INIT_CAP);
        this.cache = cache;
    }

    /**
     * Enqueues request for processing
     * @param session The request that needs processing
     */
    public void enqueue(StoreRequest session){
        if (!this.serviceQueue.contains(session))
            serviceQueue.add(session);
    }

    /**
     * Gets the service this worker is working for
     * @return The service, never null.
     */
    public StoreServiceInstance getService() {
        return service;
    }

    /**
     * Gets the amount of requests currently on the queue.
     * @return The number of requests awaiting processing
     */
    public int getQueueCount() {
        return serviceQueue.size();
    }

    /**
     * Sets the thread this worker is running on
     * @param thread The thread ( may be null )
     */
    public void setThread(Thread thread) {
        this.thread = thread;
    }

    /**
     * Stops the worker
     */
    public void shutdown() {
        if ( ! running )
            return;

        running = false;

        if ( thread != null ){
            try {
                thread.interrupt();
                thread.join();
            } catch (InterruptedException ignored) {
            }
        }

    }

    /**
     * Checks whether the worker is running, returning false if the
     * worker has not yet been started, if the worker has been shut down
     * or when the worker has died as result of an unhandled exception.
     * @return Whether the worker is running
     */
    public boolean isRunning() {
        return running;
    }

    @Override
    public void run() {
        StoreRequest request;

        running = true;

        try {
            while ( running ) {

                try {

                    //This blocks when queue is empty so no delays required here.
                    request = serviceQueue.take();

                } catch (InterruptedException e) {
                    continue;
                }

                processRequest(request);

            }
        } finally {

            running = false;

        }
    }

    private void processRequest(StoreRequest request) {
        try {
            request.setStatus(RequestStatus.PROCESSING);
            switch(request.getType()) {

                case GET_GROUP:
                    Container c = cache.read(request.getArchive(), request.getGroup());
                    request.setData(c.getData());
                    break;
                case GET_RAW:
                    if ( request.getArchive() == 255 && request.getGroup() == 255 )
                        request.setData(new Container(0,cache.createChecksumTable().encode()).encode());
                    else
                        request.setData(cache.getStore().read(request.getArchive(), request.getGroup()));
                    break;
                case GET_FILE:
                    request.setData(cache.read(request.getArchive(), request.getGroup(), request.getFile()));
                    break;
                case PUT_GROUP:
                    Container cc = cache.read(request.getArchive(), request.getGroup());
                    cc = new Container(Container.COMPRESSION_BZIP2, request.getData(), cc.getVersion()+1);
                    cache.write(request.getArchive(), request.getGroup(), cc);
                    break;
                case PUT_FILE:
                    cache.write(request.getArchive(), request.getGroup(), request.getFile(), request.getData());
                    break;

            }
            request.setStatus(RequestStatus.DONE);

        } catch ( Throwable t ) {
            request.setError(t);
        }
    }

}
