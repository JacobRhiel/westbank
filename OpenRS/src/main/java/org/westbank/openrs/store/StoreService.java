package org.westbank.openrs.store;

import org.westbank.configuration.service.Service;
import org.westbank.server.instance.ServerInstance;
import org.westbank.store.StoreInterface;

public class StoreService extends Service {

    /**
     * Path of the store
     */
    @SuppressWarnings("FieldCanBeLocal")
    private String storePath = "";

    /**
     * Creates a new service descriptor
     */
    public StoreService() {
        super(StoreInterface.NAME, -1, true);
    }

    @Override
    public Object createInstance(Object server) {
        return new StoreServiceInstance((ServerInstance) server, this);
    }

    /**
     * Gets the path of the store
     * @return the path of the store
     */
    public String getStorePath() {
        return storePath;
    }

}
