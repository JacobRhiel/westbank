package org.westbank.openrs.openrs.type;

public enum CacheIndex {

    CLIENTSCRIPT(12),
    CONFIGS(2),
    FONTS(13),
    HUFFMAN(10),
    INTERFACES(3),
    LANDSCAPES(5),
    MODELS(7),
    MUSICS1(6),
    MUSICS2(11),
    REFERENCE(255),
    ITEMS(19),
    SKELETONS(0),
    SKINS(1),
    SOUNDEFFECTS1(4),
    SOUNDEFFECTS2(14),
    SOUNDEFFECTS3(15),
    SPRITES(8),

    TEXTURES(9);

    private int id;

    CacheIndex(int id) {
        this.id = id;
    }

    public int getID() {
        return id;
    }

}
