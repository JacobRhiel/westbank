package org.westbank.openrs.openrs.type;


import org.westbank.openrs.openrs.Cache;

public interface TypeList<T extends Type> {

    void initialize(Cache cache);

    T list(int id);

    int size();

}
