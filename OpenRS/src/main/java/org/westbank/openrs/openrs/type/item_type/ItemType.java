package org.westbank.openrs.openrs.type.item_type;


import org.westbank.openrs.openrs.type.Type;
import org.westbank.openrs.openrs.utility.ByteBufferUtils;

import java.io.BufferedReader;
import java.nio.ByteBuffer;
import java.util.HashMap;

public class ItemType implements Type {

    public ItemType(int id) {
        this.id = id;
        setDefaultOptions();
        setDefaultsVariableValues();
    }

    public int id;
    private boolean loaded;

    public int modelId;
    public String name;

    // model size information
    private int modelZoom;
    private int modelRotation1;
    private int modelRotation2;
    private int modelOffset1;
    private int modelOffset2;

    // extra information
    private int stackable;
    private static int value;
    private static int tempValue;
    private boolean membersOnly;

    // wearing model information
    private int maleEquip1;
    private int femaleEquip1;
    private int maleEquip2;
    private int femaleEquip2;

    // options
    private String[] groundOptions;
    public String[] inventoryOptions;

    // model information
    public int[] originalModelColors;
    public int[] modifiedModelColors;
    public short[] originalTextureColors;
    private short[] modifiedTextureColors;
    private byte[] unknownArray1;
    private int[] unknownArray2;
    // extra information, not used for newer items
    private boolean unnoted;

    private int maleEquipModelId3;
    private int femaleEquipModelId3;
    private int unknownInt1;
    private int unknownInt2;
    private int unknownInt3;
    private int unknownInt4;
    private int unknownInt5;
    private int unknownInt6;
    private int certId;
    private int certTemplateId;
    private int[] stackIds;
    private int[] stackAmounts;
    private int unknownInt7;
    private int unknownInt8;
    private int unknownInt9;
    private int unknownInt10;
    private int unknownInt11;
    private int teamId;
    private int lendId;
    private int lendTemplateId;
    private int unknownInt12;
    private int unknownInt13;
    private int unknownInt14;
    private int unknownInt15;
    private int unknownInt16;
    private int unknownInt17;
    private int unknownInt18;
    private int unknownInt19;
    private int unknownInt20;
    private int unknownInt21;
    private int unknownInt22;
    private int unknownInt23;

    // extra added
    private boolean noted;
    private boolean lended;

    private HashMap<Integer, Object> clientScriptData;
    private HashMap<Integer, Integer> itemRequiriments;
    private int grandExchangePrice;

    public static boolean isInteger(String i) {
        try {
            Integer.parseInt(i);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public int getArchiveId() {
        return id >>> 8;
    }

    public int getFileId() {
        return 0xff & id;
    }

    public boolean isDestroyItem() {
        if (inventoryOptions == null)
            return false;
        for (String option : inventoryOptions) {
            if (option == null)
                continue;
            if (option.equalsIgnoreCase("destroy"))
                return true;
        }
        return false;
    }

    public boolean isWearItem() {
        if (inventoryOptions == null)
            return false;
        for (String option : inventoryOptions) {
            if (option == null)
                continue;
            if (option.equalsIgnoreCase("wield")
                    || option.equalsIgnoreCase("wear")
                    || option.equalsIgnoreCase("equip"))
                return true;
        }
        return false;
    }


    public boolean hasSpecialBar() {
        if (clientScriptData == null)
            return false;
        Object specialBar = clientScriptData.get(686);
        if (specialBar != null && specialBar instanceof Integer)
            return (Integer) specialBar == 1;
        return false;
    }

    public int getRenderAnimId() {
        if (clientScriptData == null)
            return 1426;
        Object animId = clientScriptData.get(644);
        if (animId != null && animId instanceof Integer)
            return (Integer) animId;
        return 1426;
    }

    public int getQuestId() {
        if (clientScriptData == null)
            return -1;
        Object questId = clientScriptData.get(861);
        if (questId != null && questId instanceof Integer)
            return (Integer) questId;
        return -1;
    }

    public HashMap<Integer, Integer> getCreateItemRequirements() {
        if (clientScriptData == null)
            return null;
        HashMap<Integer, Integer> items = new HashMap<Integer, Integer>();
        int requiredId = -1;
        int requiredAmount = -1;
        for (int key : clientScriptData.keySet()) {
            Object value = clientScriptData.get(key);
            if (value instanceof String)
                continue;
            if (key >= 538 && key <= 770) {
                if (key % 2 == 0)
                    requiredId = (Integer) value;
                else
                    requiredAmount = (Integer) value;
                if (requiredId != -1 && requiredAmount != -1) {
                    items.put(requiredAmount, requiredId);
                    requiredId = -1;
                    requiredAmount = -1;
                }
            }
        }
        return items;
    }

    public HashMap<Integer, Object> getClientScriptData() {
        return clientScriptData;
    }



	/*
	 * public HashMap<Integer, Integer> getWearingSkillRequiriments() { if
	 * (clientScriptData == null) return null; HashMap<Integer, Integer> skills
	 * = new HashMap<Integer, Integer>(); int nextLevel = -1; int nextSkill =
	 * -1; for (int key : clientScriptData.keySet()) { Object value =
	 * clientScriptData.get(key); if (value instanceof String) continue; if(key
	 * == 277) { skills.put((Integer) value, id == 19709 ? 120 : 99); }else if
	 * (key == 23 && id == 15241) { skills.put(4, (Integer) value);
	 * skills.put(11, 61); } else if (key >= 749 && key < 797) { if (key % 2 ==
	 * 0) nextLevel = (Integer) value; else nextSkill = (Integer) value; if
	 * (nextLevel != -1 && nextSkill != -1) { skills.put(nextSkill, nextLevel);
	 * nextLevel = -1; nextSkill = -1; } }
	 *
	 * } return skills; }
	 */

    private void setDefaultOptions() {
        groundOptions = new String[] { null, null, "take", null, null };
        inventoryOptions = new String[] { null, null, null, null, "drop" };
    }

    private void setDefaultsVariableValues() {
        name = "null";
        maleEquip1 = -1;
        maleEquip2 = -1;
        femaleEquip1 = -1;
        femaleEquip2 = -1;
        modelZoom = 2000;
        lendId = -1;
        lendTemplateId = -1;
        certId = -1;
        certTemplateId = -1;
        unknownInt9 = 128;
        value = 1;
        maleEquipModelId3 = -1;
        femaleEquipModelId3 = -1;
        unknownValue1 = -1;
        unknownValue2 = -1;

    }

    @Override
    public void decode(ByteBuffer stream) {
        int opcode = stream.get() & 0xFF;
        if (opcode == 1)
            modelId = stream.getShort() & 0xFFFF;
        else if (opcode == 2)
            name = ByteBufferUtils.getString(stream);
        else if (opcode == 4)
            modelZoom = stream.getShort();
        else if (opcode == 5)
            modelRotation1 = stream.getShort();
        else if (opcode == 6)
            modelRotation2 = stream.getShort();
        else if (opcode == 7) {
            modelOffset1 = stream.getShort();
            if (modelOffset1 > 32767)
                modelOffset1 -= 65536;
            modelOffset1 <<= 0;
        } else if (opcode == 8) {
            modelOffset2 = stream.getShort();
            if (modelOffset2 > 32767)
                modelOffset2 -= 65536;
            modelOffset2 <<= 0;
        } else if (opcode == 11)
            stackable = 1;
        else if (opcode == 12)
            value = stream.getInt();
        else if (opcode == 117)
            opcode117 = stream.get();
        else if (opcode == 82)
            opcode82 = stream.get();
        else if (opcode == 13)
            opcode13 = stream.get();
        else if (opcode == 14)
            opcode14 = stream.get();
        else if (opcode == 9)
            opcode9 = stream.get();
        else if (opcode == 27)
            opcode27 = stream.get();
        else if (opcode == 66)
            opcode66 = stream.get();
        else if (opcode == 116)
            opcode116 = stream.get();
        else if (opcode == 157)
            opcode157 = stream.get();
        else if (opcode == 244)
            opcode244 = stream.get();
        else if (opcode == 170)
            opcode170 = stream.get();
        else if (opcode == 151)
            opcode151 = stream.get();// 14 66 116 157 244 170 151 9
            // 27
        else if (opcode == 16)
            membersOnly = true;
        else if (opcode == 18) // added
            stream.getShort();
        else if (opcode == 23)
            maleEquip1 = stream.getShort() & 0xFFFF;
        else if (opcode == 24)
            maleEquip2 = stream.getShort() & 0xFFFF;
        else if (opcode == 25)
            femaleEquip1 = stream.getShort() & 0xFFFF;
        else if (opcode == 26)
            femaleEquip2 = stream.getShort() & 0xFFFF;
        else if (opcode >= 30 && opcode < 35)
            groundOptions[opcode - 30] = ByteBufferUtils.getString(stream);
        else if (opcode >= 35 && opcode < 40)
            inventoryOptions[opcode - 35] = ByteBufferUtils.getString(stream);
        else if (opcode == 40) {
            int length = stream.get();
            originalModelColors = new int[length];
            modifiedModelColors = new int[length];
            for (int index = 0; index < length; index++) {
                originalModelColors[index] = stream.getShort();
                modifiedModelColors[index] = stream.getShort();
            }
        } else if (opcode == 41) {
            int length = stream.get();
            originalTextureColors = new short[length];
            modifiedTextureColors = new short[length];
            for (int index = 0; index < length; index++) {
                originalTextureColors[index] = (short) stream
                        .getShort();
                modifiedTextureColors[index] = (short) stream
                        .getShort();
            }
        } else if (opcode == 42) {
            int length = stream.get();
            unknownArray1 = new byte[length];
            for (int index = 0; index < length; index++)
                unknownArray1[index] = (byte) stream.get();
        } else if (opcode == 65)
            unnoted = true;
        else if (opcode == 78)
            maleEquipModelId3 = stream.getShort() & 0xFFFF;
        else if (opcode == 79)
            femaleEquipModelId3 = stream.getShort() & 0xFFFF;
        else if (opcode == 90)
            unknownInt1 = stream.getShort() & 0xFFFF;
        else if (opcode == 91)
            unknownInt2 = stream.getShort() & 0xFFFF;
        else if (opcode == 92)
            unknownInt3 = stream.getShort() & 0xFFFF;
        else if (opcode == 93)
            unknownInt4 = stream.getShort() & 0xFFFF;
        else if (opcode == 95)
            unknownInt5 = stream.getShort();
        else if (opcode == 96)
            unknownInt6 = stream.get();
        else if (opcode == 97)
            certId = stream.getShort();
        else if (opcode == 98)
            certTemplateId = stream.getShort();
        else if (opcode >= 100 && opcode < 110) {
            if (stackIds == null) {
                stackIds = new int[10];
                stackAmounts = new int[10];
            }
            stackIds[opcode - 100] = stream.getShort();
            stackAmounts[opcode - 100] = stream.getShort();
        } else if (opcode == 110)
            unknownInt7 = stream.getShort();
        else if (opcode == 111)
            unknownInt8 = stream.getShort();
        else if (opcode == 112)
            unknownInt9 = stream.getShort();
        else if (opcode == 113)
            unknownInt10 = stream.get();
        else if (opcode == 114)
            unknownInt11 = stream.get() * 5;
        else if (opcode == 115)
            teamId = stream.get();
        else if (opcode == 121)
            lendId = stream.getShort();
        else if (opcode == 122)
            lendTemplateId = stream.getShort();
        else if (opcode == 125) {
            unknownInt12 = stream.get() << 0;
            unknownInt13 = stream.get() << 0;
            unknownInt14 = stream.get() << 0;
        } else if (opcode == 126) {
            unknownInt15 = stream.get() << 0;
            unknownInt16 = stream.get() << 0;
            unknownInt17 = stream.get() << 0;
        } else if (opcode == 127) {
            unknownInt18 = stream.get();
            unknownInt19 = stream.getShort();
        } else if (opcode == 128) {
            unknownInt20 = stream.get();
            unknownInt21 = stream.getShort();
        } else if (opcode == 129) {
            unknownInt20 = stream.get();
            unknownInt21 = stream.getShort();
        } else if (opcode == 130) {
            unknownInt22 = stream.get();
            unknownInt23 = stream.getShort();
        } else if (opcode == 132) {
            int length = stream.get();
            unknownArray2 = new int[length];
            for (int index = 0; index < length; index++)
                unknownArray2[index] = stream.getShort();
        } else if (opcode == 134) {
            int unknownValue = stream.get();
        } else if (opcode == 139) {
            unknownValue2 = stream.getShort();
        } else if (opcode == 140) {
            unknownValue1 = stream.getShort();
        } else if (opcode == 249) {
            int length = stream.get();
            if (clientScriptData == null)
                clientScriptData = new HashMap<Integer, Object>(length);
            for (int index = 0; index < length; index++) {
                boolean stringInstance = stream.get() == 1;
                int key = stream.getInt();
                Object value = stringInstance ? ByteBufferUtils.getString(stream) : stream
                        .getInt();
                clientScriptData.put(key, value);
            }
        } //else
			/*throw new RuntimeException("MISSING OPCODE " + opcode
					+ " FOR ITEM " + id);*/
    }

    @Override
    public ByteBuffer encode() {
        ByteBuffer buffer = ByteBuffer.allocate(1132);
        return (ByteBuffer) buffer.flip();
    }

    @Override
    public int getID() {
        return id;
    }

    private int opcode13;
    private int opcode82;
    private int opcode117;
    private int opcode66;
    private int opcode116;
    private int opcode157;
    private int opcode244;
    private int opcode170;
    private int opcode151;
    private int opcode14;
    private int opcode27;
    private int opcode9;
    private int unknownValue1;
    private int unknownValue2;
    private static BufferedReader reader;


    public String getName() {
        return name;
    }

    public int getFemaleWornModelId1() {
        return femaleEquip1;
    }

    public int getFemaleWornModelId2() {
        return femaleEquip2;
    }

    public int getMaleWornModelId1() {
        return maleEquip1;
    }

    public int getMaleWornModelId2() {
        return maleEquip2;
    }

    public boolean isLended() {
        return lended;
    }

    public boolean isStackable() {
        return stackable == 1;
    }

    public boolean isNoted() {
        return noted;
    }

    public int getLendId() {
        return lendId;
    }

    public int getCertId() {
        return certId;
    }

}
