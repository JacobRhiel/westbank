package org.westbank.openrs.openrs.type;

public enum ConfigArchive {

    ENUM(8),
    IDENTKIT(3),
    ITEM(53),
    INV(5),
    NPC(9),
    OBJECT(6),
    OVERLAY(4),
    SEQUENCE(12),
    SPOTANIM(13),
    UNDERLAY(1),
    VARBIT(14),
    VARCLIENT(19),
    VARCLIENTSTRING(15),
    VARPLAYER(16);

    private int id;

    ConfigArchive(int id) {
        this.id = id;
    }

    public int getID() {
        return id;
    }

}
