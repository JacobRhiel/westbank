package org.westbank.openrs.openrs;

import java.nio.ByteBuffer;

/**
 * @author JaGeX Games Studio
 */
public class Huffman {

    int[] DECRYPT_KEYS;
    int[] MASKS;
    byte[] BIT_SIZES;

    public int compress(byte[] decompressed, int dOffset, int length, byte[] compressed, int cOffset) {
        int key = 0;
        int offset = cOffset << 3;

        for (length += dOffset; dOffset < length; ++dOffset) {
            int val = decompressed[dOffset] & 255;
            int code = this.MASKS[val];
            byte size = this.BIT_SIZES[val];
            if (size == 0) {
                throw new RuntimeException("");
            }

            int position = offset >> 3;
            int bitOffset = offset & 7;
            key &= -bitOffset >> 31;
            int byteSize = position + (bitOffset + size - 1 >> 3);
            bitOffset += 24;
            compressed[position] = (byte) (key |= code >>> bitOffset);
            if (position < byteSize) {
                ++position;
                bitOffset -= 8;
                compressed[position] = (byte) (key = code >>> bitOffset);
                if (position < byteSize) {
                    ++position;
                    bitOffset -= 8;
                    compressed[position] = (byte) (key = code >>> bitOffset);
                    if (position < byteSize) {
                        ++position;
                        bitOffset -= 8;
                        compressed[position] = (byte) (key = code >>> bitOffset);
                        if (position < byteSize) {
                            ++position;
                            bitOffset -= 8;
                            compressed[position] = (byte) (key = code << -bitOffset);
                        }
                    }
                }
            }

            offset += size;
        }

        return (offset + 7 >> 3) - cOffset;
    }

    public int decompress(byte[] compressed, int cOffset, byte[] decompressed, int dOffset, int length) {
        if (length == 0) {
            return 0;
        } else {
            int position = 0;
            length += dOffset;
            int offset = cOffset;

            while (true) {
                byte character = compressed[offset];
                if (character < 0) {
                    position = this.DECRYPT_KEYS[position];
                } else {
                    ++position;
                }

                int characterId;
                if ((characterId = this.DECRYPT_KEYS[position]) < 0) {
                    decompressed[dOffset++] = (byte) (~characterId);
                    if (dOffset >= length) {
                        break;
                    }

                    position = 0;
                }

                if ((character & 0x40) != 0) {
                    position = this.DECRYPT_KEYS[position];
                } else {
                    ++position;
                }

                if ((characterId = this.DECRYPT_KEYS[position]) < 0) {
                    decompressed[dOffset++] = (byte) (~characterId);
                    if (dOffset >= length) {
                        break;
                    }

                    position = 0;
                }

                if ((character & 0x20) != 0) {
                    position = this.DECRYPT_KEYS[position];
                } else {
                    ++position;
                }

                if ((characterId = this.DECRYPT_KEYS[position]) < 0) {
                    decompressed[dOffset++] = (byte) (~characterId);
                    if (dOffset >= length) {
                        break;
                    }

                    position = 0;
                }

                if ((character & 0x20) != 0) {
                    position = this.DECRYPT_KEYS[position];
                } else {
                    ++position;
                }

                if ((characterId = this.DECRYPT_KEYS[position]) < 0) {
                    decompressed[dOffset++] = (byte) (~characterId);
                    if (dOffset >= length) {
                        break;
                    }

                    position = 0;
                }

                if ((character & 0x8) != 0) {
                    position = this.DECRYPT_KEYS[position];
                } else {
                    ++position;
                }

                if ((characterId = this.DECRYPT_KEYS[position]) < 0) {
                    decompressed[dOffset++] = (byte) (~characterId);
                    if (dOffset >= length) {
                        break;
                    }

                    position = 0;
                }

                if ((character & 0x4) != 0) {
                    position = this.DECRYPT_KEYS[position];
                } else {
                    ++position;
                }

                if ((characterId = this.DECRYPT_KEYS[position]) < 0) {
                    decompressed[dOffset++] = (byte) (~characterId);
                    if (dOffset >= length) {
                        break;
                    }

                    position = 0;
                }

                if ((character & 0x2) != 0) {
                    position = this.DECRYPT_KEYS[position];
                } else {
                    ++position;
                }

                if ((characterId = this.DECRYPT_KEYS[position]) < 0) {
                    decompressed[dOffset++] = (byte) (~characterId);
                    if (dOffset >= length) {
                        break;
                    }

                    position = 0;
                }

                if ((character & 0x1) == 0) {
                    ++position;
                } else {
                    position = this.DECRYPT_KEYS[position];
                }

                if ((characterId = this.DECRYPT_KEYS[position]) < 0) {
                    decompressed[dOffset++] = (byte) (~characterId);
                    if (dOffset >= length) {
                        break;
                    }

                    position = 0;
                }

                ++offset;
            }

            return offset + 1 - cOffset;
        }
    }

    public Huffman(ByteBuffer buffer) {
        int length = buffer.capacity();

        byte[] bytes = new byte[length];
        buffer.get(bytes);

        this.MASKS = new int[length];
        this.BIT_SIZES = bytes;
        this.DECRYPT_KEYS = new int[8];
        int[] is = new int[33];
        int var8 = 0;

        for (int index = 0; index < length; ++index) {
            byte val = bytes[index];
            if (val != 0) {
                int var6 = 1 << 32 - val;
                int var5 = is[val];
                this.MASKS[index] = var5;
                int var4;
                int var11;
                int var12;
                int var13;
                if ((var5 & var6) != 0) {
                    var12 = is[val - 1];
                } else {
                    var12 = var5 | var6;

                    for (var4 = val - 1; var4 >= 1; --var4) {
                        var13 = is[var4];
                        if (var13 != var5) {
                            break;
                        }

                        var11 = 1 << 32 - var4;
                        if ((var13 & var11) != 0) {
                            is[var4] = is[var4 - 1];
                            break;
                        }

                        is[var4] = var13 | var11;
                    }
                }

                is[val] = var12;

                for (var4 = 1 + val; var4 <= 32; ++var4) {
                    if (is[var4] == var5) {
                        is[var4] = var12;
                    }
                }

                var4 = 0;

                for (var13 = 0; var13 < val; ++var13) {
                    var11 = Integer.MIN_VALUE >>> var13;
                    if ((var5 & var11) != 0) {
                        if (this.DECRYPT_KEYS[var4] == 0) {
                            this.DECRYPT_KEYS[var4] = var8;
                        }

                        var4 = this.DECRYPT_KEYS[var4];
                    } else {
                        ++var4;
                    }

                    if (var4 >= this.DECRYPT_KEYS.length) {
                        int[] var3 = new int[2 * this.DECRYPT_KEYS.length];

                        for (int var14 = 0; var14 < this.DECRYPT_KEYS.length; ++var14) {
                            var3[var14] = this.DECRYPT_KEYS[var14];
                        }

                        this.DECRYPT_KEYS = var3;
                    }
                }

                this.DECRYPT_KEYS[var4] = ~index;
                if (var4 >= var8) {
                    var8 = 1 + var4;
                }
            }
        }

    }

}
