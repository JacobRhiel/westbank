package org.westbank.openrs.openrs.type.item_type;

import org.westbank.openrs.openrs.Archive;
import org.westbank.openrs.openrs.Cache;
import org.westbank.openrs.openrs.Container;
import org.westbank.openrs.openrs.ReferenceTable;
import org.westbank.openrs.openrs.type.CacheIndex;
import org.westbank.openrs.openrs.type.ConfigArchive;
import org.westbank.openrs.openrs.type.TypeList;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ItemTypeList implements TypeList<ItemType> {

    private ItemType[] items;

    private Logger logger = Logger.getLogger(ItemTypeList.class.getName());

    public int maxSize = 0;

    @Override
    public void initialize(Cache cache) {
        try {
            Container container = Container.decode(cache.getStore().read(CacheIndex.REFERENCE, CacheIndex.ITEMS));
            ReferenceTable table = ReferenceTable.decode(container.getData());
            Archive archive = Archive.decode(cache.read(CacheIndex.ITEMS, ConfigArchive.ITEM).getData(), table.getEntry(ConfigArchive.ITEM).size());
            items = new ItemType[archive.size()];
            for (int id = 0; id < archive.size(); id++) {
                ByteBuffer buffer = archive.getEntry(id);
                if (buffer.limit() > maxSize) maxSize = buffer.limit();
                ItemType type = new ItemType(id);
                type.decode(buffer);
                items[id] = type;
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error Loading ItemType(s)!", e);
        }
        logger.info("Loaded " + items.length + " ItemType(s)!");

		/*File file = new File("./repository/item-list.txt");
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
			for(int i = 0; i < items.length; i++) {
				writer.write(String.valueOf(i) + " - " + items[i].getName());
				writer.newLine();
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
    }

    @Override
    public ItemType list(int id) {
        //Preconditions.checkArgument(id >= 0, "ID can't be negative!");
        //Preconditions.checkArgument(id < items.length, "ID can't be greater than the max item id!");
        return items[id];
    }

    public int size() {
        return items.length;
    }

}

