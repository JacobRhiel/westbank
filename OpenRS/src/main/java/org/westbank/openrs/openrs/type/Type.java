package org.westbank.openrs.openrs.type;

import java.nio.ByteBuffer;

public interface Type {

    void decode(ByteBuffer buffer);

    ByteBuffer encode();

    int getID();

}
