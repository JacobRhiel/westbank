package org.westbank.openrs.openrs.type;


import org.westbank.openrs.openrs.Cache;
import org.westbank.openrs.openrs.type.item_type.ItemType;
import org.westbank.openrs.openrs.type.item_type.ItemTypeList;

public class TypeListManager {

    private static ItemTypeList item = new ItemTypeList();
    /*private static NpcTypeList npc = new NpcTypeList();
    private static VarBitTypeList varbit = new VarBitTypeList();
    private static VarPlayerTypeList varp = new VarPlayerTypeList();
    private static VarClientTypeList varc = new VarClientTypeList();
    private static VarClientStringTypeList varcstr = new VarClientStringTypeList();
    private static IdentkitTypeList ident = new IdentkitTypeList();
    private static UnderlayTypeList under = new UnderlayTypeList();
    private static OverlayTypeList over = new OverlayTypeList();
    private static EnumTypeList enm = new EnumTypeList();
    private static ObjectTypeList obj = new ObjectTypeList();
    private static SpotAnimTypeList anim = new SpotAnimTypeList();
    private static SequenceTypeList seq = new SequenceTypeList();*/

    public static void initialize(Cache cache) {
        item.initialize(cache);
        /*ident.initialize(cache);
        npc.initialize(cache);
        varp.initialize(cache);
        varbit.initialize(cache);
        varc.initialize(cache);
        varcstr.initialize(cache);
        under.initialize(cache);
        over.initialize(cache);
        enm.initialize(cache);
        obj.initialize(cache);
        anim.initialize(cache);
        seq.initialize(cache);*/
    }

    public static ItemType lookupItem(int id) {
        return item.list(id);
    }

    public static int itemsSize() {
        return item.size();
    }

    /*public static IdentkitType lookupIdentkit(int id) {
        return ident.list(id);
    }

    public static int identkitsSize() {
        return ident.size();
    }

    public static NpcType lookupNpc(int id) {
        return npc.list(id);
    }

    public static int npcsSize() {
        return npc.size();
    }

    public static VarBitType lookupVarBit(int id) {
        return varbit.list(id);
    }

    public static int varbitsSize() {
        return varbit.size();
    }

    public static VarPlayerType lookupVarPlayer(int id) {
        return varp.list(id);
    }

    public static int varpsSize() {
        return varp.size();
    }

    public static VarClientType lookupVarClient(int id) {
        return varc.list(id);
    }

    public static int varclientsSize() {
        return varc.size();
    }

    public static VarClientStringType lookupVarClientString(int id) {
        return varcstr.list(id);
    }

    public static int varclientstringsSize() {
        return varcstr.size();
    }

    public static UnderlayType lookupUnder(int id) {
        return under.list(id);
    }

    public static int underlaysSize() {
        return under.size();
    }

    public static OverlayType lookupOver(int id) {
        return over.list(id);
    }

    public static int overlaysSize() {
        return over.size();
    }

    public static EnumType lookupEnum(int id) {
        return enm.list(id);
    }

    public static int enumsSize() {
        return enm.size();
    }

    public static ObjectType lookupObject(int id) {
        return obj.list(id);
    }

    public static int objectsSize() {
        return obj.size();
    }

    public static SpotAnimType lookupSpotAnim(int id) {
        return anim.list(id);
    }

    public static int animsSize() {
        return anim.size();
    }

    public static SequenceType lookupSequence(int id) {
        return seq.list(id);
    }

    public static int sequencesSize() {
        return seq.size();
    }*/

}
