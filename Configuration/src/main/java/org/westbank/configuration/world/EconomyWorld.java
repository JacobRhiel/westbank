package org.westbank.configuration.world;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class EconomyWorld extends World {

    public EconomyWorld() {
        super(1, "127.0.0.1:43594", WorldStatus.BETA);
    }

}
