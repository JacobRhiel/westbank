package org.westbank.configuration.world;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public enum WorldStatus {

    DEVELOPMENTAL, PUBLIC, MEMBERS_ONLY, BETA

}
