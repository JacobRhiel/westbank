package org.westbank.configuration.world;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public abstract class World {

    public World(int world_id, String world_address, WorldStatus world_status) {
        this.world_id = world_id;
        this.world_address = world_address;
        this.world_status = world_status;
    }

    @XStreamAlias("world_id")
    private final int world_id;

    @XStreamAlias("world_address")
    private final String world_address;

    @XStreamAlias("world_status")
    private final WorldStatus world_status;

    public int getWorldID() {
        return world_id;
    }

    public String getWorldAddress() {
        return world_address;
    }

    public WorldStatus getWorldStatus() {
        return world_status;
    }

}
