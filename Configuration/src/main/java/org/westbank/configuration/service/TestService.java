package org.westbank.configuration.service;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class TestService extends Service {

    public TestService(String name, int identifier, boolean vital) {
        super(name, identifier, vital);
    }

    @Override
    public Object createInstance(Object server) {
        return null;
    }
}
