package org.westbank.configuration.service;

import com.thoughtworks.xstream.annotations.XStreamAlias;
/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public abstract class Service {

    public Service(String name, int identifier, boolean vital) {
        this.name = name;
        this.identifier = identifier;
        this.vital = vital;
    }

    @XStreamAlias("name")
    private final String name;

    public String getServiceName() { return name; }

    @XStreamAlias("identifier")
    private final int identifier;

    public int getIdentifier() { return identifier; }

    @XStreamAlias("vital")
    private boolean vital = false;

    public boolean isVital() { return vital; }

    public abstract Object createInstance(Object server);

}
