package org.westbank.configuration;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.westbank.configuration.network.NetworkConfiguration;
import org.westbank.configuration.service.Service;
import org.westbank.configuration.world.World;
import org.westbank.repository.Repository;

import java.net.URL;
import java.util.List;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public abstract class ServerConfiguration {

    public ServerConfiguration() {
        logger.info("Initializing Server Configuration..");
        stream = new XStream();
    }

    @XStreamImplicit(itemFieldName="world")
    public List<World> world_list;

    @XStreamImplicit(itemFieldName="service")
    protected List<Service> service_list;

    /**
     * Gets a list of services this server should handle
     * @return The services this server should handle
     */
    public List<World> getWorlds() {
        return world_list;
    }

    public List<Service> getServices() { return service_list; }

    @XStreamImplicit(itemFieldName = "repository")
    public List<Repository> repository_list;

    public List<Repository> getRepositories() { return repository_list; }

    public XMLConfiguration loadWorldConfiguration() {

        stream.alias("XML", XMLConfiguration.class);

        stream.processAnnotations(XMLConfiguration.class);

        stream.processAnnotations(World.class);

        world_configuration = (XMLConfiguration) stream.fromXML(getConfigurationFile("world-configuration.xml"));

        return world_configuration;
    }

    public XMLConfiguration loadRepositoryConfiguration() {

        stream.alias("XML", XMLConfiguration.class);

        stream.processAnnotations(XMLConfiguration.class);

        repository_configuration = (XMLConfiguration) stream.fromXML(getConfigurationFile("repository-configuration.xml"));

        return repository_configuration;
    }

    public XMLConfiguration loadNetworkConfiguration() {
        stream.alias("XML", XMLConfiguration.class);
        stream.processAnnotations(XMLConfiguration.class);
        stream.processAnnotations(NetworkConfiguration.class);
        network_configuration = (XMLConfiguration) stream.fromXML(getConfigurationFile("network-configuration.xml"));
        return network_configuration;
    }

    public XMLConfiguration loadServiceConfiguration() {
        stream.alias("XML", XMLConfiguration.class);
        stream.processAnnotations(XMLConfiguration.class);
        service_configuration = (XMLConfiguration) stream.fromXML(getConfigurationFile("service-configuration.xml"));
        return service_configuration;
    }

    @XStreamAlias("network")
    public NetworkConfiguration network;

    public NetworkConfiguration getNetwork() { return network; }

    private XMLConfiguration world_configuration, repository_configuration, network_configuration, service_configuration;

    public XMLConfiguration getWorldConfiguration() { return world_configuration; }

    public XMLConfiguration getRepositoryConfiguration() { return repository_configuration; }

    public XMLConfiguration getNetworkConfiguration() { return network_configuration; }

    public XMLConfiguration getServiceConfiguration() { return service_configuration; }

    protected XStream stream;

    /**
     * Gets the resource of the requested configuration file name.
     * @param file_name
     * @return the resource path of file_name.
     */
    private URL getConfigurationFile(String file_name) {
        URL file = null;
        try {
            file = this.getClass().getResource("/configuration/" + file_name);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    private Logger logger = LoggerFactory.getLogger(ServerConfiguration.class);

}
