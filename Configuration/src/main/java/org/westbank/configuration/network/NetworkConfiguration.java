package org.westbank.configuration.network;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.net.InetSocketAddress;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class NetworkConfiguration {

    @XStreamAlias("local_address")
    private String local_address;

    private String host, port;

    public InetSocketAddress getSocketAddress() {
        this.host = local_address.substring(0, local_address.indexOf(":"));
        this.port = local_address.substring(local_address.indexOf(":") + 1);
        return new InetSocketAddress(host, Integer.valueOf(port));
    }

    public String getHost() { return host; }

    public String getPort() { return port; }

}
