package org.westbank.configuration;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 *     Represents the XMLConfiguration for selectively loaded Configurations files.
 **/
public class XMLConfiguration extends ServerConfiguration {

    /**
     * Updates the XMLStream to alias our designated classes to be processed.
     */
    public XMLConfiguration() {

        world_list = new LinkedList<>();

        repository_list = new LinkedList<>();

        service_list = new LinkedList<>();

        loadWorldConfiguration();

        loadRepositoryConfiguration();

        loadNetworkConfiguration();

        loadServiceConfiguration();

    }

}
