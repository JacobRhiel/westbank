package org.westbank.game.protocol;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public enum ServerProtocol implements Protocol {

    REFLECTION_REQUEST(56, -2),
    VARBIT_SMALL(55, 3),
    VARBIT_LARGE(133, 6),
    RESET_CLIENT_VARCACHE(103, 0),
    CLIENT_SETVARCSTR_LARGE(95, -2),
    UPDATE_INV_PARTIAL(141, -2),
    UPDATE_INV_FULL(122, -2),
    UPDATE_INV_STOP_TRANSMIT(65, 3),
    SEND_PING(47, 8),
    SET_UPDATE_TIMER  (10, 2),
    UPDATE_DOB(60, 4),
    NO_TIMEOUT(144, -1),
    WORLDLIST_FETCH_REPLY(23, -2);

    private final int opcode, size;

    ServerProtocol(int opcode, int size) {
        this.opcode = opcode;
        this.size = size;
    }

    public int getOpcode() {
        return opcode;
    }

    public int getSize() {
        return size;
    }
}