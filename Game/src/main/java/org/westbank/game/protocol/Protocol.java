package org.westbank.game.protocol;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public interface Protocol {

    int VAR_SIZE_BYTE = -1;

    int VAR_SIZE_WORD = -2;

    int getOpcode();

    int getSize();

}
