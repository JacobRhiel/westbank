package org.westbank.game.session.message;

import io.netty.buffer.ByteBuf;
import org.westbank.game.protocol.Protocol;
import org.westbank.network.message.ChannelMessage;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class ClientMessage implements ChannelMessage {

    private Protocol protocol;
    private int size;
    private ByteBuf data;

    public ClientMessage(Protocol protocol, int size, ByteBuf data) {
        this.protocol = protocol;
        this.size = size;
        this.data = data;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public int getSize() {
        return size;
    }

    public ByteBuf getData() {
        return data;
    }

    @Override
    public String toString() {
        return "ClientMessage{" +
                "prot=" + protocol +
                ", size=" + size +
                ", data=" + data +
                '}';
    }
}