package org.westbank.game.session.message;

import io.netty.buffer.ByteBuf;
import org.westbank.game.protocol.Protocol;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class ServerMessage {

    public ServerMessage(Protocol protocol, int size, ByteBuf data) {
        this.protocol = protocol;
        this.size = size;
        this.data = data;
    }

    public ServerMessage(Protocol protocol, int size, ByteBuf data, boolean noCypher) {
        this.protocol = protocol;
        this.size = size;
        this.data = data;
        this.noCypher = noCypher;
    }

    public ServerMessage(Protocol protocol) {
        this.protocol = protocol;
        this.size = 0;
        this.data = null;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public int getSize() {
        return size;
    }

    public ByteBuf getData() {
        return data;
    }

    public boolean isNoCypher() {
        return noCypher;
    }

    private Protocol protocol;
    private int size;
    private ByteBuf data;
    private boolean noCypher = false;

}
