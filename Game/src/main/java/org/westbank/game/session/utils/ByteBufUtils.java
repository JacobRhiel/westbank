package org.westbank.game.session.utils;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufUtil;

import java.math.BigInteger;

public class ByteBufUtils {

    public static ByteBuf rsadec(ByteBuf in, BigInteger private_exp, BigInteger modulo) {

        int cypherTextLength = in.readUnsignedShort();

        System.out.println(cypherTextLength);

        byte[] _cypherText = new byte[cypherTextLength];
        byte[] _plainText;
        in.readBytes(_cypherText);

        if ( private_exp != null && modulo != null ) {

            BigInteger cypherText = new BigInteger(_cypherText);

            BigInteger plainText = cypherText.modPow(private_exp, modulo);

            _plainText = plainText.toByteArray();

        } else
            _plainText = _cypherText;

        return in.alloc().buffer(_plainText.length).writeBytes(_plainText);

    }

    public static String readJString(ByteBuf in) {
        //TODO: Decode JSTR charset
        byte b = 0;
        StringBuilder sb = new StringBuilder();
        while ( ( b = in.readByte() ) != 0 )
            sb.append( ( char ) b );
        return sb.toString();
    }

    public static void writeJString2(ByteBuf out, String str) {
        byte[] data = str.getBytes();
        out.writeByte(0);
        out.writeBytes(data, 0, data.length);
        out.writeByte(0);
    }

    public static ByteBuf tinydec(ByteBuf in, int size, int[] key) {
        ByteBuf out = in.alloc().buffer(size);
        int pair_count = size / 8;
        for ( int pair_ptr = 0; pair_ptr < pair_count; pair_ptr++ ) {
            int v0 = in.readInt();
            int v1 = in.readInt();
            int sum = 0xc6ef3720;
            int delta = 0x9e3779b9;
            int i_59_ = 32;
            while ( i_59_-- > 0 ) {
                v1 -= ((v0 >>> 5 ^ v0 << 4) + v0 ^ key[(sum >> 11) & 3] + sum);
                sum -= delta;
                v0 -= ((v1 >>> 5 ^ v1 << 4) + v1 ^ sum + key[sum & 0x3]);
            }
            out.writeInt(v0);
            out.writeInt(v1);
        }
        return out;
    }

    public static void psmarts(ByteBuf buf, int i) {
        if ( i >= 0 && i < 128 )
            buf.writeByte(i);
        else if ( i >= 0 && i < 32768 )
            buf.writeShort(i + 32768);
        else {
            throw new IllegalArgumentException("Value out of range");
        }
    }
}
