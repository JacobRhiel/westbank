package org.westbank.game.session.decoder;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public enum GameDecoderState {
    AWAIT_OPCODE,
    AWAIT_SIZE,
    AWAIT_DATA, DO_NOTHING,
}