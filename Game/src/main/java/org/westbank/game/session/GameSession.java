package org.westbank.game.session;

import io.netty.channel.Channel;
import org.westbank.game.opcode.OpcodeDecoder;
import org.westbank.game.opcode.OpcodeEncoder;
import org.westbank.game.opcode.OpcodePreparer;
import org.westbank.game.protocol.Protocol;
import org.westbank.game.service.GameServiceInstance;
import org.westbank.game.session.crypto.Isaac;
import org.westbank.game.session.decoder.GameDecoderState;
import org.westbank.game.session.message.ClientMessage;
import org.westbank.network.message.ServiceMessage;
import org.westbank.network.session.Session;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class GameSession extends Session {

    public GameSession(Channel channel, GameServiceInstance service, OpcodePreparer opcode_preparer) {
        super(channel);
        this.service = service;
        this.opcode_preparer = opcode_preparer;
        channel.pipeline().addBefore("ChannelHandler", "enc", new OpcodeEncoder());
        channel.pipeline().addBefore("ChannelHandler", "dec", new OpcodeDecoder());
    }

    @Override
    public void messageReceived(Object _message) {

        if ( !( _message instanceof ClientMessage ) )
            throw new InternalError("Programmer error!: GameSession received non game message");

        ClientMessage message = (ClientMessage) _message;

        Protocol protocol = message.getProtocol();

        opcode_preparer.handlePacket( this, message );

    }

    public void initialized(ServiceMessage msg) {

        int id = msg.getServiceIdentifier();

        Protocol protocol = getPacketHandler().findProtocol(id);

        setNextPacket( protocol );

        if ( protocol.getSize() == 0 )
            messageReceived( new ClientMessage( protocol, 0, getChannel().alloc().buffer(0,0) ) );
        else if ( protocol.getSize() < 0)
            setDecoderState( GameDecoderState.AWAIT_SIZE );
        else
            setDecoderState( GameDecoderState.AWAIT_DATA );

    }

    public Isaac getInboundCypher() {
        return inboundCypher;
    }

    public Isaac getOutboundCypher() {
        return outboundCypher;
    }

    public GameDecoderState getDecoderState() {
        return decoderState;
    }

    public void setDecoderState(GameDecoderState loginState) {
        this.decoderState = loginState;
    }

    public void setNextPacket(Protocol prot) {

        this.nextPacket = prot;

        if ( prot.getSize() >= 0 )
            nextSize = prot.getSize();

    }

    public Protocol getNextPacket() {
        return nextPacket;
    }

    public void setNextSize(int nextSize) {
        this.nextSize = nextSize;
    }

    public int getNextSize() {
        return nextSize;
    }

    public OpcodePreparer getPacketHandler() {
        return opcode_preparer;
    }

    public void setPacketHandler( OpcodePreparer handler ) {
        this.opcode_preparer = handler;
    }

    public void setCypher(Isaac inbound, Isaac outbound) {
        inboundCypher = inbound;
        outboundCypher = outbound;
    }

    private final GameServiceInstance service;

    private OpcodePreparer opcode_preparer;

    private Isaac inboundCypher;
    private Isaac outboundCypher;

    private GameDecoderState decoderState = GameDecoderState.DO_NOTHING;
    private int nextSize;
    private Protocol nextPacket;

}
