package org.westbank.game.session.utils;

import java.math.BigInteger;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class ServerKeys {

    public static final BigInteger GAME_MODULO = null;
    public static final BigInteger GAME_PRIVATE_EXP = null;

}