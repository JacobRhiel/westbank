package org.westbank.game.service;

import io.netty.channel.Channel;
import org.westbank.configuration.service.Service;
import org.westbank.game.opcode.OpcodePreparer;
import org.westbank.game.session.GameSession;
import org.westbank.server.instance.ServerInstance;
import org.westbank.server.instance.ServiceInstance;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class GameServiceInstance extends ServiceInstance {

    public GameServiceInstance(ServerInstance server, Service service, OpcodePreparer opcode_preparer) {
        super(server, service);
        this.opcode_preparer = opcode_preparer;
    }

    @Override
    public Object createSession(Channel channel) {
        return new GameSession(channel, this, opcode_preparer);
    }

    public OpcodePreparer getInitialPacketHandler() {
        return opcode_preparer;
    }

    @Override
    public void shutdown() {

    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {

    }

    private OpcodePreparer opcode_preparer;

}
