package org.westbank.game.opcode;

import org.westbank.game.protocol.ServerProtocol;
import org.westbank.game.session.message.ServerMessage;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class BasicOpcodeEncoder {

    public static ServerMessage encodeNoTimeout(){
        return new ServerMessage(ServerProtocol.NO_TIMEOUT);
    }

}
