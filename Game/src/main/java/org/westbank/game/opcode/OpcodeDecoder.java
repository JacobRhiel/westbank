package org.westbank.game.opcode;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.westbank.game.protocol.Protocol;
import org.westbank.game.session.GameSession;
import org.westbank.game.session.crypto.Isaac;
import org.westbank.game.session.decoder.GameDecoderState;
import org.westbank.game.session.message.ClientMessage;
import org.westbank.network.channel.ChannelHandler;
import org.westbank.network.session.Session;
import sun.plugin.dom.exception.InvalidStateException;

import java.util.List;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class OpcodeDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

        Session _session = ctx.channel().attr(ChannelHandler.session_key).get();

        if ( _session == null || ! (_session instanceof GameSession) )
            throw new InternalError("Programmer error!: GameInitDecoder on non-game Channel");

        GameSession session = (GameSession) _session;

        Isaac cypher = session.getInboundCypher();

        int opcode, required;
        Protocol prot;

        switch( session.getDecoderState() ){

            case AWAIT_SIZE:

                prot = session.getNextPacket();

                if ( prot.getSize() == Protocol.VAR_SIZE_BYTE ) {

                    if ( !in.isReadable( 1 ) )
                        return;

                    session.setNextSize(in.readUnsignedByte());

                } else if ( prot.getSize() == Protocol.VAR_SIZE_WORD ) {

                    if ( !in.isReadable( 2 ) )
                        return;

                    session.setNextSize(in.readUnsignedShort());

                } else
                    throw new InvalidStateException( "Invalid state, AWAIT_SIZE with size: " + prot.getSize() );

                session.setDecoderState(GameDecoderState.AWAIT_DATA);

                break;

            case AWAIT_DATA:

                prot = session.getNextPacket();
                required = session.getNextSize();

                if ( !in.isReadable( required ) )
                    return;

                ByteBuf data = ctx.alloc().buffer( required );
                in.readBytes( data, required );

                out.add( new ClientMessage( prot, required, data ) );

                session.setDecoderState(GameDecoderState.AWAIT_OPCODE);

                break;

            case AWAIT_OPCODE:

                if ( !in.isReadable( 1 ) )
                    return;

                opcode = in.readUnsignedByte();

                if ( cypher != null )
                    opcode = ( opcode - session.getInboundCypher().next() ) & 0xFF;

                prot = session.getPacketHandler().findProtocol( opcode );

                session.setNextPacket(prot);

                if ( prot.getSize() == 0) {

                    session.setDecoderState(GameDecoderState.AWAIT_OPCODE);

                    out.add(new ClientMessage(prot, 0, ctx.alloc().buffer( 0 )));

                } else if ( prot.getSize() > 0 )
                    session.setDecoderState(GameDecoderState.AWAIT_DATA);
                else
                    session.setDecoderState(GameDecoderState.AWAIT_SIZE);

                break;

        }
    }
}