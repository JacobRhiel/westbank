package org.westbank.game.opcode;

import org.westbank.game.protocol.Protocol;
import org.westbank.game.session.GameSession;
import org.westbank.game.session.message.ClientMessage;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public interface OpcodePreparer {

    Protocol findProtocol(int opcode);

    void handlePacket(GameSession session, ClientMessage message);

}
