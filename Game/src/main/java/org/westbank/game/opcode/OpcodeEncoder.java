package org.westbank.game.opcode;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.westbank.game.protocol.Protocol;
import org.westbank.game.session.GameSession;
import org.westbank.game.session.crypto.Isaac;
import org.westbank.game.session.message.ServerMessage;
import org.westbank.network.channel.ChannelHandler;
import org.westbank.network.session.Session;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class OpcodeEncoder extends MessageToByteEncoder<ServerMessage> {

    @Override
    protected void encode(ChannelHandlerContext ctx,
                          ServerMessage msg,
                          ByteBuf byteBuf) throws Exception {

        Session _session = ctx.channel().attr(ChannelHandler.session_key).get();

        if ( _session == null || ! (_session instanceof GameSession) )
            throw new InternalError("Programmer error!: GameEncoder on non-game Channel");

        GameSession session = (GameSession) _session;

        Isaac cypher = msg.isNoCypher() ? null : session.getOutboundCypher();

        Protocol prot = msg.getProtocol();

        int opcode = prot.getOpcode();

        if ( opcode >= 128) {

            int high = ( opcode >> 8 ) & 0xFF;
            int low = opcode & 0xFF;

            high -= 128;

            if ( cypher != null ) {
                high += cypher.next();
                low += cypher.next();
            }

            byteBuf.writeByte( high );
            byteBuf.writeByte(low);
        } else {

            if ( cypher != null )
                opcode += cypher.next();

            byteBuf.writeByte(opcode);
        }


        if ( prot.getSize() == Protocol.VAR_SIZE_BYTE )
            byteBuf.writeByte( msg.getSize() );
        else if ( prot.getSize() == Protocol.VAR_SIZE_WORD )
            byteBuf.writeShort( msg.getSize() );

        ByteBuf data = msg.getData();

        if ( data != null )
            byteBuf.writeBytes( data );

    }
}