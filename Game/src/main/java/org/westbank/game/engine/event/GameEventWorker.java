package org.westbank.game.engine.event;

import org.westbank.game.login.LoginServiceInstance;

import java.util.concurrent.PriorityBlockingQueue;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class GameEventWorker implements Runnable {

    public GameEventWorker(LoginServiceInstance login_service) {
        this.login_service = login_service;
    }

    @Override
    public void run() {

        GameEvent event;

        thread_active = true;

        try {
            while ( thread_active ) {

                event = game_event_queue.peek();

                if ( event == null ) {
                    try {
                        Thread.sleep(999999);//TODO: Find cleaner way to do this
                    } catch ( InterruptedException ignored ) {}
                    continue;
                }

                long time = event.getExecutionTime() - System.currentTimeMillis();

                if ( time > 0 ) {
                    try {
                        Thread.sleep( time );
                    } catch ( InterruptedException ignored ) {}
                    continue;
                } else if ( time <= 0 && latency < -time )
                    latency = -time;

                game_event_queue.remove( event );

                processEvent( event );

            }
        } finally {

            thread_active = false;

        }
    }

    private void processEvent(GameEvent event) {

        long t = System.currentTimeMillis();
        event.getDelegate().run();
        t = System.currentTimeMillis() - t;
        //TODO: Check elapsed time

        if ( event.isRepetitive() ) {
            event.doRepeat();
            login_service.enqueue(event);
        }

    }

    public void enqueue(GameEvent event) {
        if(!this.game_event_queue.contains(event)) {
            this.game_event_queue.add(event);
        }
    }

    public int getQueueCount() {
        return game_event_queue.size();
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public LoginServiceInstance getService() {
        return login_service;
    }

    public boolean isThreadActive() {
        return thread_active;
    }

    private volatile boolean thread_active;

    private Thread thread = null;

    private volatile long latency;

    private PriorityBlockingQueue<GameEvent> game_event_queue = new PriorityBlockingQueue<>();

    private LoginServiceInstance login_service;

}
