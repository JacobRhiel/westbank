package org.westbank.game.engine.event;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class GameEvent implements Comparable<GameEvent> {

    public GameEvent(long execution_time, Runnable delegate) {
        this.execution_time = execution_time;
        this.delegate = delegate;
    }

    public GameEvent(long delay, boolean repetitive, Runnable delegate) {
        this.execution_time = System.currentTimeMillis() + delay;
        this.repetitive = repetitive;
        this.updated_execution_time = delay;
        this.delegate = delegate;
    }

    @Override
    public int compareTo(GameEvent o) {
        return (int)(execution_time - o.execution_time);
    }

    public void doRepeat() {
        execution_time = System.currentTimeMillis() + updated_execution_time;
    }

    private boolean repetitive;

    private long execution_time, updated_execution_time;

    private int cycles;

    private final Runnable delegate;

    public boolean isRepetitive() {
        return repetitive;
    }

    public long getExecutionTime() {
        return execution_time;
    }

    public long getUpdatedExecutionTime() {
        return updated_execution_time;
    }

    public Runnable getDelegate() {
        return delegate;
    }


}
