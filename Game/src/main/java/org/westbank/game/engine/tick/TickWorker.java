package org.westbank.game.engine.tick;

import org.westbank.game.login.LoginServiceInstance;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class TickWorker implements Runnable {

    public TickWorker(LoginServiceInstance login_service) {
        this.login_service = login_service;
    }

    public static final long cyclic_rate = 600;

    @Override
    public void run() {

    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    private Thread thread = null;

    private LoginServiceInstance login_service;

}

