package org.westbank.game.login.opcode;

import org.westbank.game.login.decoder.LoginDecoder;
import org.westbank.game.login.response.ResponseProtocol;
import org.westbank.game.login.session.LoginSession;
import org.westbank.game.opcode.OpcodePreparer;
import org.westbank.game.protocol.Protocol;
import org.westbank.game.session.GameSession;
import org.westbank.game.session.decoder.GameDecoderState;
import org.westbank.game.session.message.ClientMessage;
import org.westbank.game.session.message.ServerMessage;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class LoginOpcodeHandler implements OpcodePreparer {

    @Override
    public Protocol findProtocol(int opcode) {
        return LoginProtocol.findByOpcode( opcode );
    }

    @Override
    public void handlePacket(GameSession _session, ClientMessage message) {

        final LoginSession session = (LoginSession) _session;

        LoginProtocol prot = (LoginProtocol) message.getProtocol();

        System.out.println("msg: " + message);

        switch( prot ) {

            case INIT_GAME_CONNECTION:
                session.setDecoderState(GameDecoderState.AWAIT_OPCODE);
                session.getChannel().writeAndFlush( new ServerMessage(ResponseProtocol.INIT_CONTINUE));
                break;

            case INIT_SOCIAL_CONNECTION:

                break;

            case GAMELOGIN:
                System.out.println("Game login attempt.");
                //TODO: Pipeline...
                LoginDecoder login_decoder = new LoginDecoder(message.getData());

                break;

            case LOBBY_LOGIN:

                break;

        }

    }

}
