package org.westbank.game.login.session.message;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.westbank.game.session.utils.ByteBufUtils;
import org.westbank.game.session.utils.ServerKeys;
import org.westbank.game.utilities.BufferUtils;
import org.westbank.game.utilities.XTEA;
import org.westbank.network.message.ChannelMessage;
import sun.plugin.dom.exception.InvalidStateException;

import java.util.Arrays;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class LoginMessage implements ChannelMessage {

    protected int affiliate;
    protected boolean reconnecting;
    protected int clientVersion;
    protected byte[] randomData;
    protected String settings;
    protected String username;
    protected String password;
    protected int[] isaacSeed = new int[4];
    protected long serverSessionKey;
    protected long clientSessionKey;

    public int getAffiliate() {
        return affiliate;
    }

    public boolean isReconnecting() {
        return reconnecting;
    }

    public int getClientVersion() {
        return clientVersion;
    }

    public byte[] getRandomData() {
        return randomData;
    }

    public String getSettings() {
        return settings;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public long getServerSessionKey() {
        return serverSessionKey;
    }

    public long getClientSessionKey() {
        return clientSessionKey;
    }

    @Override
    public String toString() {
        return "LoginMessage{" +
                "affiliate=" + affiliate +
                ", reconnecting=" + reconnecting +
                ", clientVersion=" + clientVersion +
                ", randomData=" + Arrays.toString(randomData) +
                ", settings='" + settings + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", isaacSeed=" + Arrays.toString(isaacSeed) +
                ", serverSessionKey=" + serverSessionKey +
                ", clientSessionKey=" + clientSessionKey +
                '}';
    }

    public int[] getIsaacSeed() {
        return isaacSeed;
    }
}
