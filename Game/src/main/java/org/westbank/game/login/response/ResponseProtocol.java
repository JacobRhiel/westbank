package org.westbank.game.login.response;

import org.westbank.game.protocol.Protocol;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public enum ResponseProtocol implements Protocol {

    INIT_CONTINUE(0),
    STATUS_OK(2, Protocol.VAR_SIZE_BYTE),
    INVALID_USERNAME_OR_PASSWORD(3),
    BANNED(4),
    DUPLICATE(5),
    CLIENT_OUT_OF_DATE(6),
    SERVER_FULL(7),
    LOGINSERVER_OFFLINE(8),
    IP_LIMIT(9),
    BAD_SESSION(10),
    FORCE_PASSWORD_CHANGE(11),
    NEED_MEMBERS_ACCOUNT(12),
    INVALID_SAVE(13),
    UPDATE_IN_PROGRESS(14),
    RECONNECT_OK(15),
    TOO_MANY_ATTEMPTS(16),
    IN_MEMBERS_AREA(17),
    LOCKED(18),
    FULLSCREEN_P2P(19),
    INVALID_LOGIN_SERVER(20),
    HOP_BLOCKED(21),
    INVALID_LOGIN_PACKET(22),
    NO_LOGIN_RESPONSE(23),
    LOGINSERVER_LOAD_ERROR(24),
    UNKNOWN_REPLY_FROM_LOGINSERVER(25),
    IP_BLOCKED(26);

    private final int opcode;
    private final int size;

    ResponseProtocol(int opcode, int size) {
        this.opcode = opcode;
        this.size = size;
    }

    public int getOpcode() {
        return opcode;
    }

    public int getSize() { return size; }

    ResponseProtocol(int opcode) {
        this.opcode = opcode;
        this.size = 0;
    }


}