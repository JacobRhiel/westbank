package org.westbank.game.login;

import org.westbank.configuration.service.Service;
import org.westbank.server.instance.ServerInstance;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class LoginService extends Service {

    public LoginService() {
        super("Login Service", 14, true);
    }

    @Override
    public Object createInstance(Object server) {
        return new LoginServiceInstance((ServerInstance) server, this);
    }

}
