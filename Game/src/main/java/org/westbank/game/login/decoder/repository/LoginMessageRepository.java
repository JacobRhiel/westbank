package org.westbank.game.login.decoder.repository;

import org.westbank.game.login.decoder.message.LoginBlockMessage;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class LoginMessageRepository {

    public LoginMessageRepository() {

    }

    public void instiantateRepository() {
        login_message_checklist = new LinkedList<>();
        login_block_map = new ConcurrentHashMap<>();
    }

    public List<LoginBlockMessage> getLoginBlockChecklist() {
        return login_message_checklist;
    }

    public void addLoginBlockCheck(String desc, LoginBlockMessage message) {
        if(login_message_checklist.contains(message) || login_block_map.contains(message)) return;
        login_message_checklist.add(message);
        login_block_map.put(desc, message);
    }

    public LoginBlockMessage getLoginBlockMessage(LoginBlockMessage message) {
        return login_message_checklist.get(login_message_checklist.indexOf(message));
    }

    public LoginBlockMessage getLoginBlockMessage(String desc) {
        return login_block_map.get(desc);
    }

    private List<LoginBlockMessage> login_message_checklist;

    private ConcurrentHashMap<String, LoginBlockMessage> login_block_map;

}
