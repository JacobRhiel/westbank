package org.westbank.game.login.decoder.message.impl;

import io.netty.buffer.ByteBuf;
import org.westbank.game.login.decoder.message.LoginBlockMessage;
import org.westbank.game.session.utils.ByteBufUtils;
import org.westbank.game.session.utils.ServerKeys;

import java.math.BigInteger;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class RSAMessage implements LoginBlockMessage {

    public RSAMessage() {

    }

    @Override
    public void decodeMessage(ByteBuf buf) {

        BigInteger private_exp = ServerKeys.GAME_PRIVATE_EXP;

        BigInteger modulo = ServerKeys.GAME_MODULO;

        if(!buf.isReadable() || buf.readableBytes() <= 0) return;

        int cypherTextLength = buf.readUnsignedShort();

        byte[] _cypherText = new byte[cypherTextLength];

        byte[] _plainText;

        buf.readBytes(_cypherText);

        if ( private_exp != null && modulo != null ) {

            BigInteger cypherText = new BigInteger(_cypherText);

            BigInteger plainText = cypherText.modPow(private_exp, modulo);

            _plainText = plainText.toByteArray();

        } else
            _plainText = _cypherText;

        rsa_block_buffer = buf.alloc().buffer(_plainText.length).writeBytes(_plainText);

        acceptMessageDecoding();

    }

    @Override
    public boolean acceptMessageDecoding() {

        int magic_number = rsa_block_buffer.readUnsignedByte();

        int block = rsa_block_buffer.readUnsignedByte();

        if(magic_number != 1) {
            System.out.println("RSA Magic # is INVALID.");
            return false;
        }

        for ( int i = 0; i < isaac_key.length; i++ )
            isaac_key[i] = rsa_block_buffer.readInt();

        if (block == 1 || block == 3) {
            int code = rsa_block_buffer.readUnsignedMedium();
            rsa_block_buffer.readerIndex(rsa_block_buffer.readerIndex() + 5);
        } else if (block == 0) {
            int trusted = rsa_block_buffer.readInt();
            rsa_block_buffer.readerIndex(rsa_block_buffer.readerIndex() + 4);
        } else if (block == 2) {
            rsa_block_buffer.readerIndex(rsa_block_buffer.readerIndex() + 8);
        }

        password = ByteBufUtils.readJString(rsa_block_buffer);

        return true;
    }

    @Override
    public int setMessageSize() {
        return 0;
    }

    private ByteBuf rsa_block_buffer;

    private int[] isaac_key = new int[4];

    public int[] getIsaacKey() {
        return isaac_key;
    }

    private String password;

    public String getPassword() { return password; }

}
