package org.westbank.game.login.opcode;

import org.westbank.game.protocol.Protocol;
import sun.plugin.dom.exception.InvalidStateException;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public enum LoginProtocol implements Protocol {

    INIT_GAME_CONNECTION(14, 0),
    INIT_JS5REMOTE_CONNECTION(15, 4),
    GAMELOGIN(16, -2),
    r(17, 0),
    LOBBY_LOGIN(19, -2),
    CREATE_ACCOUNT(22, -2),
    REQUEST_WORLDLIST(23, 4),
    CHECK_WORLD_SUITABILITY(24, -1),
    GAMELOGIN_CONTINUE(26, 0),
    SSL_WEBCONNECTION(27, 0),
    CREATE_CHECK_EMAIL(28, -2),
    INIT_SOCIAL_CONNECTION(29, -2),
    SOCIAL_NETWORK_LOGIN(30, -2);

    private static Map<Integer, LoginProtocol> LOOKUP_TABLE;

    private final int opcode;
    private final int size;

    public static LoginProtocol findByOpcode( int opcode ) {

        if ( LOOKUP_TABLE == null ) {
            LOOKUP_TABLE = new HashMap<>();
            for (LoginProtocol c : values())
                LOOKUP_TABLE.put(c.getOpcode(), c);
        }

        LoginProtocol p = LOOKUP_TABLE.get(opcode);

        if ( p == null )
            throw new InvalidStateException("Unknown opcode!");

        return p;
    }

    LoginProtocol(int opcode, int size) {
        this.opcode = opcode;
        this.size = size;
    }

    public int getOpcode() {
        return opcode;
    }

    public int getSize() {
        return size;
    }
}