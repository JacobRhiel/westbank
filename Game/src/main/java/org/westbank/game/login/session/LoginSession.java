package org.westbank.game.login.session;

import io.netty.channel.Channel;
import org.westbank.game.engine.tick.TickWorker;
import org.westbank.game.opcode.BasicOpcodeEncoder;
import org.westbank.game.opcode.OpcodePreparer;
import org.westbank.game.protocol.Protocol;
import org.westbank.game.service.GameServiceInstance;
import org.westbank.game.session.GameSession;
import org.westbank.game.session.crypto.Isaac;
import org.westbank.game.login.LoginServiceInstance;
import org.westbank.game.login.session.message.LoginMessage;
import org.westbank.game.session.message.ClientMessage;

import java.util.Arrays;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class LoginSession extends GameSession {

    public LoginSession(Channel channel,
                     GameServiceInstance service,
                     OpcodePreparer initialPacketHandler) {
        super(channel, service, initialPacketHandler);
        this.opcode_preparer = initialPacketHandler;
        login_service = (LoginServiceInstance) service;
    }

    @Override
    public void messageReceived(Object msg) {

        System.out.println("login session msg: " + msg);

        if ( !( msg instanceof ClientMessage) )
            throw new InternalError("Programmer error!: GameSession received non game message");

        ClientMessage message = (ClientMessage) msg;

        Protocol prot = message.getProtocol();

        opcode_preparer.handlePacket( this, message );

    }

    public void initCypher(LoginMessage msg) {

        int inKeys[], outKeys[];

        outKeys = msg.getIsaacSeed();

        inKeys = Arrays.copyOf(outKeys, outKeys.length);

        for ( int i = 0; i < inKeys.length; i++ )
            inKeys[i] += 50;

        setCypher(new Isaac(outKeys), new Isaac(inKeys));

    }

    public void onTick(long time) {

        if ( ( time - lastKeepalive ) > KEEPALIVE_TIMEOUT || !getChannel().isOpen() ) {
            login_service.closeSession(this);
            return;
        }

        if (sessionType == SessionType.LOGIN)
            return;

        getChannel().write(BasicOpcodeEncoder.encodeNoTimeout());

        getChannel().flush();

    }

    public void updateLastKeepalive() {
        lastKeepalive = System.currentTimeMillis();
    }

    private enum SessionType {
        LOBBY,
        GAME,
        LOGIN
    }

    private static final long KEEPALIVE_TIMEOUT = TickWorker.cyclic_rate * 4;

    private long lastKeepalive;

    private OpcodePreparer opcode_preparer;

    private SessionType sessionType = SessionType.LOGIN;

    private LoginServiceInstance login_service;

}
