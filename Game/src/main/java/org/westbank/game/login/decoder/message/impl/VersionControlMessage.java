package org.westbank.game.login.decoder.message.impl;

import io.netty.buffer.ByteBuf;
import org.westbank.game.login.decoder.message.LoginBlockMessage;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class VersionControlMessage implements LoginBlockMessage {

    public VersionControlMessage() {

    }

    @Override
    public void decodeMessage(ByteBuf buf) {

        if(!buf.isReadable()) return;

        if(buf.readableBytes() <= 0) return;

        message_build = buf.readInt();

        acceptMessageDecoding();

    }

    @Override
    public boolean acceptMessageDecoding() {
        if(message_build == BUILD) return true;
        return false;
    }


    @Override
    public int setMessageSize() {
        return 0;
    }

    private int message_build;

    private final int BUILD = 120; //TODO: Temporary.

}
