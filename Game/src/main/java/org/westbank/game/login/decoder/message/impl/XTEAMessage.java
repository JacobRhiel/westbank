package org.westbank.game.login.decoder.message.impl;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.westbank.game.login.decoder.message.LoginBlockMessage;
import org.westbank.game.utilities.BufferUtils;
import org.westbank.game.utilities.XTEA;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class XTEAMessage implements LoginBlockMessage {

    public XTEAMessage(int[] isaac_key) {
        this.isaac_key = isaac_key;
    }

    @Override
    public void decodeMessage(ByteBuf buf) {

        byte[] xtea = new byte[buf.readableBytes()];

        buf.readBytes(xtea);

        ByteBuf xteaBuf = Unpooled.wrappedBuffer(new XTEA(xtea).decrypt(isaac_key).toByteArray());

        username = BufferUtils.getString(xteaBuf);

        int display = xteaBuf.readUnsignedByte();
        int width = xteaBuf.readUnsignedShort();
        int height = xteaBuf.readUnsignedShort();

        byte[] uid = new byte[24];
        for (int i = 0; i < uid.length; i++)
            uid[i] = xteaBuf.readByte();

        String token = BufferUtils.getString(xteaBuf);

        xteaBuf.readInt();//affiliate id

        xteaBuf.readUnsignedByte();// 6
        xteaBuf.readUnsignedByte();// OS Type
        xteaBuf.readUnsignedByte();// 64-Bit OS
        xteaBuf.readUnsignedByte();// OS Version
        xteaBuf.readUnsignedByte();// Java Vendor
        xteaBuf.readUnsignedByte();// Something todo with Java
        xteaBuf.readUnsignedByte();// Something todo with Java
        xteaBuf.readUnsignedByte();
        xteaBuf.readUnsignedByte();// 0
        xteaBuf.readUnsignedShort();// Max Mem
        xteaBuf.readUnsignedByte();// Availible Processors
        xteaBuf.readUnsignedMedium();// 0
        xteaBuf.readUnsignedShort();// 0
        BufferUtils.getJagString(xteaBuf);// usually null
        BufferUtils.getJagString(xteaBuf);// usually null
        BufferUtils.getJagString(xteaBuf);// usually null
        BufferUtils.getJagString(xteaBuf);// usually null
        xteaBuf.readUnsignedByte();
        xteaBuf.readUnsignedShort();
        BufferUtils.getJagString(xteaBuf);// usually null
        BufferUtils.getJagString(xteaBuf);// usually null
        xteaBuf.readUnsignedByte();
        xteaBuf.readUnsignedByte();

        int[] var = new int[3];
        for (int i = 0; i < var.length; i++)
            var[i] = xteaBuf.readInt();

        xteaBuf.readInt();
        xteaBuf.readUnsignedByte();

        int[] crc = new int[xteaBuf.readableBytes() / 4];
        for (int i = 0; i < crc.length; i++)
            crc[i] = xteaBuf.readInt();

        acceptMessageDecoding();
    }

    @Override
    public boolean acceptMessageDecoding() {
        return false;
    }

    @Override
    public int setMessageSize() {
        return 0;
    }

    private int[] isaac_key;

    private String username;

    public String getUsername() {
        return username;
    }

}
