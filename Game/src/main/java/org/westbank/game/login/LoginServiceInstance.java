package org.westbank.game.login;

import io.netty.channel.Channel;
import org.westbank.configuration.service.Service;
import org.westbank.game.engine.event.GameEvent;
import org.westbank.game.engine.event.GameEventWorker;
import org.westbank.game.engine.tick.TickWorker;
import org.westbank.game.service.GameServiceInstance;
import org.westbank.game.login.opcode.LoginOpcodeHandler;
import org.westbank.game.login.session.LoginSession;
import org.westbank.network.session.Session;
import org.westbank.server.instance.ServerInstance;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class LoginServiceInstance extends GameServiceInstance {

    public LoginServiceInstance(ServerInstance server, Service service) {
        super(server, service, new LoginOpcodeHandler());

        System.out.println("loginservice");

        game_event_workers = new GameEventWorker[event_worker_count];

        for ( int i = 0; i < event_worker_count; i++ )
            startEventWorker(i);

        tick_worker = new TickWorker(this);
        Thread thread = createThread( tick_worker );
        thread.setName("RS Tick Worker");
        tick_worker.setThread(thread);
        thread.start();

    }

    private GameEventWorker startEventWorker(int i) {
        GameEventWorker worker = new GameEventWorker(this);
        Thread thread = createThread( worker );
        thread.setName("RS Event Worker #"+i);
        worker.setThread(thread);
        thread.start();
        game_event_workers[i] = worker;
        return worker;
    }

    @Override
    public Session createSession(Channel channel) {
        LoginSession session = new LoginSession(channel, this, getInitialPacketHandler());
        do
            try {
                createdSessions.put(session);
                break;
            } catch (InterruptedException ignored ) { }
        while (true);
        return session;
    }

    public void enqueue(GameEvent event) {
        GameEventWorker l = null;
        int _c,c = Integer.MAX_VALUE;

        for ( GameEventWorker w : game_event_workers ) {
            if ( w == null || !w.isThreadActive())
                continue;
            _c = w.getQueueCount();
            if ( _c < c ) {
                c = _c;
                l = w;
            }
        }

        if ( l == null )
            l = startEventWorker(0);//THIS SHOULD NEVER EVER EVER HAPPEN

        l.enqueue(event);

    }

    public void closeSession( LoginSession session ) {
        do
            try {
                closedSessions.put(session);
                break;
            } catch (InterruptedException ignored ) { }
        while (true);
    }

    private BlockingQueue<LoginSession> createdSessions = new LinkedBlockingQueue<>();

    private BlockingQueue<LoginSession> closedSessions = new LinkedBlockingQueue<>();

    private List<LoginSession> sessionList = new LinkedList<>();

    public List<LoginSession> getSessions() {
        return sessionList;
    }

    public BlockingQueue<LoginSession> getCreatedSessions() {
        return createdSessions;
    }

    public BlockingQueue<LoginSession> getClosedSessions() {
        return closedSessions;
    }

    @Override
    public void shutdown() {

    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {

    }

    private int event_worker_count = 1;

    private GameEventWorker game_event_workers[];

    private TickWorker tick_worker;

}
