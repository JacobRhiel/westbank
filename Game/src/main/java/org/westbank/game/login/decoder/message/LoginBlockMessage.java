package org.westbank.game.login.decoder.message;

import io.netty.buffer.ByteBuf;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public interface LoginBlockMessage {

    void decodeMessage(ByteBuf buf);

    boolean acceptMessageDecoding();

    int setMessageSize();


}
