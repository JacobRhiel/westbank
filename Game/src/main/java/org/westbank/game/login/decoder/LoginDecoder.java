package org.westbank.game.login.decoder;

import io.netty.buffer.ByteBuf;
import org.westbank.game.login.decoder.message.LoginBlockMessage;
import org.westbank.game.login.decoder.message.impl.RSAMessage;
import org.westbank.game.login.decoder.message.impl.VersionControlMessage;
import org.westbank.game.login.decoder.message.impl.XTEAMessage;
import org.westbank.game.login.decoder.repository.LoginMessageRepository;

/**
 * @author Jacob Rhiel<Jacob.Rhiel@hotmail.com>
 **/
public class LoginDecoder {

    public LoginDecoder(ByteBuf data) {

        LoginMessageRepository login_message_repository = new LoginMessageRepository();

        login_message_repository.instiantateRepository();

        login_message_repository.addLoginBlockCheck("version_control", new VersionControlMessage());

        login_message_repository.addLoginBlockCheck("rsa", new RSAMessage());

        int[] isaac_key = ((RSAMessage)login_message_repository.getLoginBlockMessage("rsa")).getIsaacKey();

        login_message_repository.addLoginBlockCheck("xtea", new XTEAMessage(isaac_key));

        for(LoginBlockMessage message : login_message_repository.getLoginBlockChecklist()) message.decodeMessage(data);

        System.out.println(((XTEAMessage) login_message_repository.getLoginBlockMessage("xtea")).getUsername());

        System.out.println(((RSAMessage)login_message_repository.getLoginBlockMessage("rsa")).getPassword());

    }


}
